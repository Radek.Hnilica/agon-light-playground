; $Id: p.hex16.asm,v 1.2 2023-08-04 19:15:15+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/p.hex16.asm,v $
;---------------------------------------------------------------
; P.HEX16 - print 16-bit number in HL in hexadecimal
; IN:	HL - 16 bit word to print
; OUT:	screen
; DESTROY: AF
; DEPENDS: P.HEX8
; NOTE: Z80 safe
;
P.HEX16:
	LD	A,H
	CALL	P.HEX8
	LD	A,L
	;pass to P.HEX8
	;.INCLUDE "/src/asmlib/P.HEX8.ASM"

; CALLS:
;P.HEX8:
