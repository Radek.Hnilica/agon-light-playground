; $Id: isxdigit.asm,v new 2023-07-28 00:14:26+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/mosmon/examine.asm,v $
;
; Is character hexadecimal digit?
; Copyright (c) 2023 Radek Hnilica
;
; BUILD:
;	ez80asm isxdigit.asm -lsv
;
; USAGE
;	LD	A,'E'
;	CALL	ISXDIGIT
;---------------------------------------------------------------
; ISXDIGIT - is the character in A hexadecimal digit?
; IN:	A
; OUT:	F.Cy - 0=no, 1=yes
; DESTROY: -
;
ISXDIGIT:
	CP	'0'		; if (A<'0')
	JR	C,@NO		;+ YES:  go @NO
	CP	'9'+1		; if (A<='9')
	RET	C		;+ YES: return Cy=1
	CP	'A'		; if (A<'A')
	JR	C,@NO		;+ YES:  go @NO
	CP	'F'+1		; if (A<='F')
	RET	C		;+ YES: return Cy=1
	SCF			; Correct Carry for case
@NO:	CCF			; Fix the Carry by negating
	RET			; Cy contains 0

