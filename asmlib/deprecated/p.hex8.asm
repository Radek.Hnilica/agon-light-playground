; $Id: p.hex8.asm,v 1.2 2023-08-04 15:46:22+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/p.hex8.asm,v $
;---------------------------------------------------------------
; P.HEX8 -- Print byte in A as two digit hexadecimal number
; IN:	A - byte to print
; OUT:	screen
; DESTROY: A,F
; DEPENDS: -
;
P.HEX8:
	PUSH	AF	; Remember A
	RRA		; Put high nible to low nible
	RRA		;+
	RRA		;+
	RRA		;+
	CALL	P.HEX4	; Print the high nible
	POP	AF	; Recover A, we need low nible
	;pass through	; print low nible
P.HEX4:	AND	$0F	; Mask low bits
	ADD	A,$90	
	DAA
	ADC	A,$40
	DAA
	RST.L	$10	;.VDU ; print char in A
	RET

