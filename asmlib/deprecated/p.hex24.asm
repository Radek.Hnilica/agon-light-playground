; $Id: p.hex24.asm,v 1.2 2023-08-04 19:16:00+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/p.hex24.asm,v $
;---------------------------------------------------------------
; P.HEX24 - print HL as 24-bit hexadecimal number
; IN:	HL - 24 bit word to print
; OUT:	screen
; DESTROY: A,F
; DEPENDS: P.HEX8, P.HEX16
; NOTE: ADL only
;
P.HEX24:
	PUSH	IX
	PUSH	HL		; Get MSB
	LD	IX,0		;+
	ADD	IX,SP		;+
	LD	A,(IX+2)	;+
	CALL	P.HEX8
	POP	HL
	POP	IX
	;pass to P.HEX16
	;.INCLUDE "/src/asmlib/P.HEX16.ASM"

; CALLS
;P.HEX16:
;P.HEX8:
