; $Id: toupper.asm,v new 2023-07-28 00:14:26+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/mosmon/examine.asm,v $
;
; Convert ASCII character to upper case
; Copyright (c) 2023 Radek Hnilica
;
; BUILD:
;	ez80asm toupper.asm -lsv
;
; USAGE
;	LD	A,'b'
;	CALL	TOUPPER
;	VDU
;---------------------------------------------------------------
; TOUPPER - convert character in A to upper case
; IN:	A
; OUT:	A
; DESTROY: F
;
TOUPPER:
	CP	'a'		; is ACC < 'a'
	RET	C		;+ YES: return result
	CP	'z'+1		;  NO: is ACC <= 'z'
	RET	NC		;+   NO: return result
	RES	5,A		;+   YES: a-z => A-Z
	RET

