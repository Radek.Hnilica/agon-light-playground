; $Id: p.nl.asm,v 1.3 2023-08-04 13:35:05+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/p.nl.asm,v $
;---------------------------------------------------------------
; P.NL - Print New Line (CRLF)
; IN:	-
; OUT:	screen
; DESTROY: AF
; DEPENDS: -
; NOTE:	Z80 safe
;
P.NL:
;@ENTR: PUSH	AF		; Save used registers
	LD	A,'\r'		; Output CR using
	RST.L	$10		;+VDU char call
	LD	A,'\n'		; Output LF using
	RST.L	$10		;+VDU char call
;@LEAV: POP	AF		; Restore used registers
	RET
