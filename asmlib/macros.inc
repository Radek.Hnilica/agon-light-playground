; $Id: macros.inc,v 1.1 2023-08-07 11:57:18+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/macros.inc,v $
;
; Macros for MOS
;

;--------------------------------------------------------------
; MOS - call MOS api function fn
; IN:	fn - function
;
	.MACRO	MOS fn
	LD	A,fn
	RST.L	8
	.ENDMACRO

;--------------------------------------------------------------
; VDU character interface
; IN:	A - character to sent to VDP
;
	.MACRO	VDU
	RST.L	16
	.ENDMACRO

;--------------------------------------------------------------
; VDU character 
; IN:	char - character to sent to VDP
;
	.MACRO	VDU.C char
	LD	A,char
	RST.L	16
	.ENDMACRO

;--------------------------------------------------------------
; MOS functions
getkey:		.EQU	0	; Read a key-press vrom VDP
sysvars:	.EQU	8	; Fetch a pointer to the system variables

