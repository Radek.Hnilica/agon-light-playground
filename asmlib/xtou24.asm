; $Id: xtou24.asm,v new 2023-07-28 00:14:26+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/mosmon/examine.asm,v $
;
; Convert ASCII string of hexadecimal digits to 24-bit unsigned integer
; Copyright (c) 2023 Radek Hnilica
;
; BUILD:
;	ez80asm atou24.asm -lsv
;
; USAGE
;	LD	IX,str
;	CALL	XTOU24
;	CALL	P.HEX24
;
;---------------------------------------------------------------
; ATOU24 - convert sequence of ASCII HEX characters to number
; IN:	IX - ^memory
; OUT:	HL - 24bit number, IX
; DESTROY:
; USE:	
;
XTOU24:
	LD	HL,0		; Initialize number to zero
	LD	B,8		; No more than 8 digits
@LOOP:

	LD	A,(IX+0)	; Get character from CLI
	CALL	TOUPPER
	CALL	ISXDIGIT	; Is it hexadecimal digit?
	RET	NC		;+NO: Exit point, HL=result
	CALL	CH2HEX4		; Convert to nible
	;CALL	P.HEX8S		; And show the result

	; Now push the nible in A into HL
	ADD	HL,HL		; Shift Left one nible
	ADD	HL,HL		;+
	ADD	HL,HL		;+
	ADD	HL,HL		;+
	OR	L		; Insert the nible into HL
	LD	L,A		;+
	INC	IX		; Next character
	DJNZ	@LOOP

	RET

;---------------------------------------------------------------
; CHTOHEX - Convert hexadecimal character to number
; IN:	A - character
; OUT:	A - number
;
CH2HEX4:
	SUB	'0'
	CP	10		; if < 10
	JR	C,@F		;+ YES: exit	
	SUB	7		;+ NO: fix it
@@:	RET

