; $Id: u24toub8.inc,v 1.2 2023-08-07 21:44:40+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/u24toub8.inc,v $
; Unsigned Int 24 to BCD
;---------------------------------------------------------------
; U24TOUB8 - convert unsigned 24 to unpacked BCD 8 digits
; IN:	HL - number to convert
; OUT:	HL - address of BCD unpacked BCD 8 bytes
; DESTROY:
; USE:
; DEPENDS: -
;
U24TOUB8:
	PUSH	AF		; Save used registers
	PUSH	BC		;+
	PUSH	IX		;+

	LD	(@BIN),HL	; Store u24 to @BIN

	; Erase BCD
	LD	B,8
	XOR	A,A		; A=0
	LD	IX,@BCD
@@:	LD	(IX+0),A
	INC	IX
	DJNZ	@B


	LD	B,24		; 24-bits in (BIN) to convert
@CLOOP:
	; ADD 3

	LD	C,8
	LD	HL,@BCD
@ADDLOOP:
	LD	A,(HL)		; A<-digit
	CP	5		; IF A<5
	JR	C,@F		;  YES: skip addition
	ADD	A,3		;  NO A>=5 THEN ADD 3
	LD	(HL),A		;+ 
@@:
	INC	HL		; next digit
	DEC	C		; till we process all digits
	JR	NZ,@ADDLOOP

	;Shift binary part
	LD	HL,@BIN
	SLA	(HL)		; Shift binary number
	INC	HL		;+
	RL	(HL)		;+
	INC	HL		;+
	RL	(HL)		;+
	; Shift BCD part
	LD	C,8		; 7 digits in
	LD	HL,@BCD		;+BCD
@SHIFT_BCD:			; Cy from BIN or prev. BCD digit
	LD	A,(HL)		
	RLA			
	BIT	4,A		; Z=b4
	JR	Z,@F
	AND	$0F
	SCF			; Cy=b4
@@:	LD	(HL),A
	INC	HL		; Next BCD Digit
	DEC	C
	JR	NZ,@SHIFT_BCD

	DJNZ	@CLOOP		; Process all 24 bits

	LD	HL,@BCD

@LEAV:	POP	IX		; Restore used registers
	POP	BC		;+
	POP	AF		;+
	RET

; DATA for U24TOBCD
;
@BINPTR:.BLKP	1		; pointer to BIN data
@BCDPTR:.BLKP	1		; pointer to unpacked BCD data

@BIN:	.BLKB	3		; 3 byte binary number
@BCD:	.BLKB	8		; eight BCD digits for output

;*TODO: move BIN & BCD out an get pointer parameters
