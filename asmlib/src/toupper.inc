; $Id: toupper.inc,v 1.1 2023-08-08 18:13:49+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/toupper.inc,v $
;
; Convert ASCII character to upper case
; Copyright (c) 2023 Radek Hnilica
;
; USAGE
;	LD	A,'b'
;	CALL	TOUPPER
;	VDU
;---------------------------------------------------------------
; TOUPPER - convert character in A to upper case
; IN:	A
; OUT:	A
; DESTROY: F
;
TOUPPER:
	CP	'a'		; is ACC < 'a'
	RET	C		;+ YES: return result
	CP	'z'+1		;  NO: is ACC <= 'z'
	RET	NC		;+   NO: return result
	RES	5,A		;+   YES: a-z => A-Z
	RET

