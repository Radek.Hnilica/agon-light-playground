; $Id: p.mem8.inc,v 1.1 2023-08-04 15:34:51+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/p.mem8.asm,v $
;---------------------------------------------------------------
; P.MEM8 - print content of memory given by HL in hexadecimal
;       memory is preinted as sequence of space separated bytes
; IN:	HL - address of memory
;       B - count / number of bytes to print
; OUT:	screen, HL
; DESTROY: AF, B
; DEPENDS: P.HEX8
;
P.MEM8:
@ENTR:	PUSH	AF		; save used registers
@LOOP:	.VDU.C	' '		; print space as separator
	LD	A,(HL)		; get byte
	CALL	P.X8		; print it in hex
	INC	HL		; advance HL to next byte
	DJNZ	@LOOP		; till B not zero, loop
@LEAV:	POP	AF		; restore saved registers
	RET
