; $Id: p.x16.inc,v 1.3 2023-08-07 21:34:26+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.x16.inc,v $
;---------------------------------------------------------------
; P.X16 - print 16-bit number in HL in hexadecimal
; IN:	HL - 16 bit word to print
; OUT:	screen
; DESTROY: AF
; DEPENDS: P.X8, P.X4
; NOTE: Z80 safe
;
P.X16:
	LD	A,H
	CALL	P.X8
	LD	A,L
	;pass to P.X8
	;.INCLUDE "/lib/asm/P.X8.ASM"
