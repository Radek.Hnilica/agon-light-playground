; $Id: p.cstr.inc,v 1.2 2023-08-07 12:08:55+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.cstr.inc,v $
;---------------------------------------------------------------
; P.CSTR - print zero terminated (C-Lang) string at address HL
; IN:	HL - string address
; OUT:	screen
; DESTROY: - 
;
P.CSTR:
	PUSH	AF		; Save used registers
	PUSH	BC		;+
	LD	BC,0		; use delimiter
	XOR	A		; 0 as delimiter
	RST.L	$18		; VDU string
	POP	BC		; Restore used registers
	POP	AF		;+
	RET

