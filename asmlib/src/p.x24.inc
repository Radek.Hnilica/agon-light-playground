; $Id: p.x24.inc,v 1.3 2023-08-07 21:34:44+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.x24.inc,v $
;---------------------------------------------------------------
; P.X24 - print HL as 24-bit hexadecimal number
; IN:	HL - 24 bit word to print
; OUT:	screen
; DESTROY: A,F
; DEPENDS: P.X16, P.X8, P.X4
; NOTE: ADL only
;
P.X24:
	PUSH	IX
	PUSH	HL		; Get MSB
	LD	IX,0		;+
	ADD	IX,SP		;+
	LD	A,(IX+2)	;+
	CALL	P.X8
	POP	HL
	POP	IX
	;pass to P.X16
	;.INCLUDE "/lib/asm/p.x16.inc"

