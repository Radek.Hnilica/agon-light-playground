; $Id: p.nl.s.asm,v 1.2 2023-08-04 13:35:57+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/p.nl.s.asm,v $
;---------------------------------------------------------------
; P.NL.S - Print New Line (CRLF) without modifying registers
; IN:	-
; OUT:	screen
; DESTROY: -
; DEPENDS: P.NL
; NOTE:	Z80 safe
;
P.NL.S:	PUSH	AF		; Save used registers
	CALL	P.NL		; Do the New Line
	POP	AF		; Restore used registers
	RET
