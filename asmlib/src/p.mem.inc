; $Id: p.mem.asm,v 1.1 2023-08-04 15:34:06+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/p.mem.asm,v $
;---------------------------------------------------------------
; P.MEM - print content of memory given by HL in hexadecimal
;       memory is printed as sequence of non separated bytes
; IN:	HL - address of memory
;       B - count / number of bytes to print
; OUT:	screen, HL
; DESTROY: AF, B
; DEPENDS: P.X8, P.X4
;
P.MEM:
@ENTR:	PUSH	AF
@LOOP:	LD	A,(HL)		; get byte
	CALL	P.X8		; print it
	INC	HL		; advance HL to next byte
	DJNZ	@LOOP		;
@LEAV:	POP	AF
	RET
	;INCLUDE "/lib/asm/p.x8.inc
