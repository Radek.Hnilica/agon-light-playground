# Library of routines and functions for eZ80 in ADL mode

Because of continuously retyping code for many subroutines,
I finally decided to create library of these for easy use.
The library is in form of `.asm` files which can be directly
included.  And also will be part of my documentation for Agon
Light computer as listings.

# Structure of the library
Each routine, with few exceptions, is in its own file which is
named after the routine itself.  I also follow naming
conventions which make more wasy to recognize the routine and
its functionality from its name.

Naming conventions:
- `P.<name>` -- subroutines which print something on the screen
- `P.HEX<number>` -- print number as hexadecimal.
- `C.` -- read from keyboard

# List of the files/routines

- `P.CSTR`	-- print zero terminated (C-lang) string
- `P.NL`	-- print newline (CRLF)
- `P.NL.S`	-- register safe version of P.NL
- `P.X4`	-- print nibble as hexadecimal digit
- `P.X8`	-- print byte as two digit hexadecimal number
- `P.X16`	-- print 16-bit word in hexadecimal
- `P.X24`	-- print 24-bit word in hexadecimal
- `P.MEM`	-- print memory in non separated bytes
- `P.MEMR`	-- as P.MEM only the output is in reverse
- `P.MEM8`	-- print memory as space separated bytes
- `U24TOUB8`	-- convert 24-bit binary to 8 digit unpacked BCD
- `UB8TOU24`	-- convert 8 digit unpacked BCD to 24-bit binary
- `ISXDIGIT`	-- is character a hexadecimal digit?
- `TOUPPER'	-- convert character to uppercase

# Legacy / deprecated

- P.HEX8.ASM  -- print A in hexadecimal.  Also contain P.HEX4 routine.
- P.HEX16.ASM
- P.HEX24.ASM
- P.STR.ASM
- P.NL.ASM
- SHOW.REGS.ASM

Planed Implementation:
- atou24 -- convert decimal number in ascii into unsigned integer

