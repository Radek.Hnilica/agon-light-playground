; $Id: ub8tou24.inc,v new date time developer $
; $Source: path/to/the/file $
;---------------------------------------------------------------
; UB8TOU24 - Convert unpacked BCD to unsigned 24-bit integer
; IN:	HL ->BCD 8 unpacked BCD digits, one per byte in LSB
; OUT:	HL - BIN 24-bit unsigned integer
; DESTROY: <list of registers whose values are not preserved>
; DEPENDS: <list of routnes, memory addresses defined elsewhere>
;
UB8TOU24:
@ENTR:	PUSH	AF
	PUSH	BC
	PUSH	DE

	; Copy ^HL to BCD
	LD	B,8
	LD	DE,@BCD
@@:	LD	A,(HL)
	LD	(DE),A
	INC	DE
	INC	HL
	DJNZ	@B

	; Cleanup BIN
	.IF 0
;	; 11B of code
;	LD	HL,@BIN
;	LD	B,3
;	XOR	A,A
;@@:	LD	(HL),A
;	INC	HL
;	DJNZ	@B
	.ENDIF

	; Alternate cleanup code
	; 10B of code
	LD	HL,@BIN
	XOR	A,A
	LD	(HL),A
	INC	HL
	LD	(HL),A
	INC	HL
	LD	(HL),A


	;.VDU.C	' '
	;.VDU.C	' '
	;.VDU.C	'='
	;CALL	@SHOW_MEM
	;CALL	P.NL

	LD	C,24		; Bit counter
@BITLOOP:

	;LD	A,C
	;CALL	P.X8
	;.VDU.C	'>'

	; Shift Right BCD
	LD	B,8		; Shift right 8 BCD digits
	LD	HL,@BCD+7	; from Most Significant one
	SCF			; Initialise Carry to 0	
	CCF			;+
@SHIFT_BCD:
	LD	A,(HL)		; We rotate in A because there
				; is more to do.	
	JR	NC,@F		; Copy Carry to
	SET	4,A		; bit 4
@@:	SRL	A		;
	LD	(HL),A
	; Carry hold LSB

	DEC	HL
	DJNZ	@SHIFT_BCD

	; Shift Right BIN2+2
	LD	HL,@BIN+2
	RR	(HL)
	DEC	HL
	RR	(HL)
	DEC	HL
	RR	(HL)

	;CALL	@SHOW_MEM

	; Subtract 3 if >= 8
	LD	HL,@BCD		; we can go throught digits in
	LD	B,8		; any direction.  Chosing LSB.
@SUBTRACT:
	LD	A,(HL)		; get the digit
	CP	7
	JR	C,@F
	SUB	3
	LD	(HL),A		; push the updated digit back
@@:	INC	HL
	DJNZ	@SUBTRACT

	;CALL	SHOW.REGS
	;CALL	P.NL
	;.VDU.C	' '
	;.VDU.C	' '
	;.VDU.C	'-'
	;CALL	@SHOW_MEM
	;CALL	P.NL

	DEC	C
	JR	NZ,@BITLOOP

	LD	HL,(@BIN)	; Get the result to return
@LEAV:	POP	DE
	POP	BC
	POP	AF
	RET

;--------------------------------------------------------------
; DATA for UB8TOU24
@BIN:	.BLKB	3
@BCD:	.BLKB	8


;--------------------------------------------------------------
; For Debugging.  Excluded from production code by .IF 0
;	.IF	0
;@SHOW_MEM:
;	PUSH	BC
;	PUSH	HL
;
;	LD	HL,@BCD
;	LD	B,8
;	CALL	P.MEMR
;
;	.VDU.C	'|'
;
;	LD	HL,@BIN
;	LD	B,3
;	CALL	P.MEMR
;
;	POP	HL
;	POP	BC
;	RET
;	.ENDIF

