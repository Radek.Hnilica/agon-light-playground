; $Id: p.memr.inc,v 1.1 2023-08-07 21:41:07+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.memr.inc,v $
;---------------------------------------------------------------
; P.MEMR - print content of memory given by HL in hexadecimal
;       memory is printed as sequence of non separated bytes
; IN:	HL - address of memory
;       B - count / number of bytes to print
; OUT:	screen, HL
; DESTROY: AF, B
; DEPENDS: P.X8, P.X4
;
P.MEMR:
@ENTR:	PUSH	AF
	PUSH	HL
	LD	DE,0
	LD	E,B
	DEC	DE
	ADD	HL,DE
@LOOP:	LD	A,(HL)		; get byte
	CALL	P.X8		; print it
	DEC	HL		; advance HL to next byte
	DJNZ	@LOOP		;
@LEAV:	POP	HL
	POP	AF
	RET
	;INCLUDE "/lib/asm/p.x8.inc
