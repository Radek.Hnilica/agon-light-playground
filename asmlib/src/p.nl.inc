; $Id: p.nl.inc,v 1.4 2023-08-07 21:12:41+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.nl.inc,v $
;---------------------------------------------------------------
; P.NL - Print New Line (CRLF)
; IN:	-
; OUT:	screen
; DESTROY: AF
; DEPENDS: -
; NOTE:	Z80 safe
;
P.NL:
;@ENTR: PUSH	AF		; Save used registers
	.VDU.C	'\r'
	.VDU.C	'\n'
;@LEAV: POP	AF		; Restore used registers
	RET
