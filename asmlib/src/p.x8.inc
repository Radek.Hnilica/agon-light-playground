; $Id: p.x8.inc,v 1.3 2023-08-07 21:29:02+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.x8.inc,v $
;---------------------------------------------------------------
; P.X8 - Print byte in A as two digit hexadecimal number
; IN:	A - byte to print
; OUT:	screen
; DESTROY: A,F
; DEPENDS: P.X4
;
P.X8:
	PUSH	AF		; Remember A
	RRA			; Put high nible to low nible
	RRA			;+
	RRA			;+
	RRA			;+
	CALL	P.X4		; Print the high nible
	POP	AF		; Recover A, we need low nible
	;pass to P.X4		; print low nible

	;IMPORT /lib/asm/P.X4.INC

