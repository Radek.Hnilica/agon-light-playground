; $Id: p.x4.inc,v 1.3 2023-08-07 21:25:05+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.x4.inc,v $
;---------------------------------------------------------------
; P.X4 -- Print low nibble in A as hexadecimal digit
; IN:	A - nibble to print
; OUT:	screen
; DESTROY: A,F
; DEPENDS: -
;
P.X4:
	AND	$0F		; Mask low bits
	ADD	A,$90	
	DAA
	ADC	A,$40
	DAA
	.VDU			; print ASCII xdigit
	RET

