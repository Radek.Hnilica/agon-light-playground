# ASMLIB

Library of subroutines for eZ80asm.
Some of these are specific to Agon Quark MOS, some are more universal.

- Source files are in folder `src/`
- Testing programs for subroutines are in `test/`

This folder contains:
- `macros.inc` -- couple of macros for MOS api
- *.sh -- maintenance scripts I use in the development process
- *.asm -- old library files.  These are deprecated and will be removed as soon
  as all programs using them are updated to use /lib/asm


# Installations
When the library in `src/` is tested succesfully by test program in `test/`,
I manually copy it to /lib/asm/.  It is done manually because I do development
on Agon computer itself.
