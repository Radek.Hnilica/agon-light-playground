#!/bin/bash
# $Id: pull.sh,v new $
# $Source: asmlib $
# Pull source, listing, symbols, and binary from sd card or its backup.
# Copyright (c) 2023 Radek Hnilica

#set -xe
declare CPOPTS=
declare ROPTS="-acv --delete --delete-after"

declare -r SD=/media/radek/AEDE-11B1
declare -r BKP=$HOME/st/repo/agonl2/backup
declare -r PROJ=asmlib

# Copy files
cp $CPOPTS $BKP/src/${PROJ}/src/*.inc $BKP/src/${PROJ}/src/README.md src/
rsync $ROPTS $BKP/src/${PROJ}/deprecated/ deprecated/
cp $CPOPTS $BKP/src/${PROJ}/test/{*.asm,*.lst,README.md} test/
cp $CPOPTS $BKP/src/${PROJ}/*.inc $BKP/src/${PROJ}/README.md .
