; $Id: file,v new date time developer $
; $Source: path/to/the/file $
;---------------------------------------------------------------
; <subroutine-name> - <subroutine-description>
; IN:	<inputs: registers, memory, keyboard, ...> description
; OUT:	<outputs: registers, memory, screen, ...>
; DESTROY: <list of registers whose values are not preserved>
; DEPENDS: <list of routnes, memory addresses defined elsewhere>
;
<sub-name>:
@ENTR:	PUSH	registers
	... subroutine code
@LEAV:	POP	registers
	RET

; DATA:
	<data local to subroutine>

