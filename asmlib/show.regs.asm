; $Id: show.regs.asm,v 1.1 2023-07-31 19:12:22+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/show.regs.asm,v $
;
;---------------------------------------------------------------
; SHOW.REGS -- show content of some CPU registers
;
SHOW.REGS:
	CALL	SHOW.AF
	CALL	SHOW.BCU
	CALL	SHOW.DEU
	CALL	SHOW.HLU
	CALL	SHOW.IXU
	CALL	SHOW.IYU
	CALL	P.NL
	RET

;---------------------------------------------------------------
; SHOW.AF -- Show registers A & F
;
SHOW.AF:
	PUSH	AF
	PUSH	HL
	LD	HL,@M.SHOW.AF
	CALL	P.CSTR
	POP	HL
	POP	AF
	PUSH	AF		; AF->HL
	POP	HL		;+
	CALL	P.HEX16
	RET
@M.SHOW.AF:
	.ASCIZ	" AF:"

;---------------------------------------------------------------
; SHOW.BCU -- Show register BCU as 24-bit number
;
SHOW.BCU:
	PUSH	HL
	LD	HL,@M.SHOW.BCU
	CALL	P.CSTR
	LD	HL,0
	ADD	HL,BC
	CALL	P.HEX24
	POP	HL
	RET
@M.SHOW.BCU:
	.ASCIZ	" BCU:"

;---------------------------------------------------------------
; SHOW.DEU -- Show register DEU as 24-bit number
;
SHOW.DEU:
	PUSH	HL
	LD	HL,@M.SHOW.DEU
	CALL	P.CSTR
	LD	HL,0
	ADD	HL,DE
	CALL	P.HEX24
	POP	HL
	RET
@M.SHOW.DEU:
	.ASCIZ	" DEU:"

;---------------------------------------------------------------
; SHOW.HLU -- Show register HLU as 24-bit number
;
SHOW.HLU:
	PUSH	HL
	LD	HL,@M.SHOW.HLU
	CALL	P.CSTR
	CALL	P.HEX24
	POP	HL
	RET
@M.SHOW.HLU:
	.ASCIZ	" HLU:"

;---------------------------------------------------------------
; SHOW.IXU -- Show register IXU as 24-bit number
;
SHOW.IXU:
	PUSH	HL
	LD	HL,@M.SHOW.IXU
	CALL	P.CSTR
	PUSH	IX
	POP	HL
	CALL	P.HEX24
	POP	HL
	RET
@M.SHOW.IXU:
	.ASCIZ	" IXU:"

;---------------------------------------------------------------
; SHOW.IYU -- Show register IYU as 24-bit number
;
SHOW.IYU:
	PUSH	HL
	LD	HL,@M.SHOW.IYU
	CALL	P.CSTR
	PUSH	IY
	POP	HL
	CALL	P.HEX24
	POP	HL
	RET
@M.SHOW.IYU:
	.ASCIZ	" IYU:"

	;.INCLUDE "/src/asmlib/P.HEX24.ASM"
	;.INCLUDE "/src/asmlib/P.HEX16.ASM"
	;.INCLUDE "/src/asmlib/P.HEX8.ASM"
	;.INCLUDE "/src/asmlib/P.NL.ASM"
	;.INCLUDE "/src/asmlib/P.CSTR.ASM"
