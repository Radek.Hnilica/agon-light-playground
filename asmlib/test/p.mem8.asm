; $Id: p.mem8.asm,v new date time developer $
; $Source: path/to/the/file $
; Testing subroutine P.MEM8
;---------------------------------------------------------------
;
;BUILD & INSTALL:
;	ez80asm p.mem8.asm /mos/test.p.mem8.bin -lv
;
;RUN:
;	test.p.mem8
;
	.INCLUDE "/lib/asm/macros.inc"


	.FILLBYTE 0
	.ASSUME	ADL=1		; eZ80 ADL
	.ORG	$0B0000		; where the MOS commands lives
	JR	START		; where the real code lives

	; MOS program header
	.ALIGN	$40		; Header is at offset 64 ($40)
	.DB	"MOS",0,1	; MOS, version 0, 1=ADL mode


;--------------------------------------------------------------
; START - the real code starts here
;
START:
	PUSH	IY		; Save IY or face consequences

	LD	HL,WELCOM
	CALL	P.CSTR

	LD	HL,WELCOM
	LD	B,8
	CALL	P.MEM8

	CALL	P.NL

	LD	HL,0		; return success
	POP	IY
	RET

;--------------------------------------------------------------
; PROGRAM DATA
WELCOM:	.ASCII	"Testing P.MEM8\r\n"
	.ASCII	"$Id: p.mem.asm8,v new, 2023-08-07, 18:00 radek $\r\n"
	.ASCIZ	"Testing implementation of P.MEM8\r\n"

;--------------------------------------------------------------
; INCLUDE needed libraries
;
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "../src/p.nl.inc"
	.INCLUDE "../src/p.mem8.inc"
	.INCLUDE "../src/p.x8.inc"
	.INCLUDE "../src/p.x4.inc"
