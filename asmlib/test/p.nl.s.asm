; $Id: p.nl.s.asm,v 1.1 2023-08-07 23:04:42+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/test/p.nl.s.asm,v $
; Testing subroutine P.NL
;---------------------------------------------------------------
;
;BUILD & INSTALL:
;	ez80asm p.nl.s.asm /mos/test.p.nl.s.bin -lv
;
;RUN:
;	test.p.nl.s
;
	.INCLUDE "/lib/asm/macros.inc"


	.FILLBYTE 0
	.ASSUME	ADL=1		; eZ80 ADL
	.ORG	$0B0000		; where the MOS commands lives
	JR	START		; where the real code lives

	; MOS program header
	.ALIGN	$40		; Header is at offset 64 ($40)
	.DB	"MOS",0,1	; MOS, version 0, 1=ADL mode


;--------------------------------------------------------------
; START - the real code starts here
;
START:
	PUSH	IY		; Save IY or face consequences

	LD	HL,WELCOM
	CALL	P.CSTR

	.VDU.C	'#'
	CALL	P.NL.S
	CALL	P.NL.S

	LD	HL,0		; return success
	POP	IY
	RET

;--------------------------------------------------------------
; PROGRAM DATA
WELCOM:	.ASCII	"Testing P.NL.S\r\n"
	.ASCIZ	"$Id: p.nl.s.asm,v 1.1 2023-08-07 23:04:42+02 radek Exp $\r\n"

;--------------------------------------------------------------
; INCLUDE needed libraries
;
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "/lib/asm/p.nl.inc"
	.INCLUDE "../src/p.nl.s.inc"

