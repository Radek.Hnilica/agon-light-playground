; $Id: isxdigit.asm,v 1.1 2023-08-08 18:14:27+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/test/isxdigit.asm,v $
; Testing subroutine P.X4
;---------------------------------------------------------------
;
;BUILD & INSTALL:
;	ez80asm isxdigit.asm /mos/test.isxdigit.bin -lv
;
;RUN:
;	test.isxdigit
;
	.INCLUDE "/lib/asm/macros.inc"

	.FILLBYTE 0
	.ASSUME	ADL=1		; eZ80 ADL
	.ORG	$0B0000		; where the MOS commands lives
	JR	START		; where the real code lives

	; MOS program header
	.ALIGN	$40		; Header is at offset 64 ($40)
	.DB	"MOS",0,1	; MOS, version 0, 1=ADL mode


;--------------------------------------------------------------
; START - the real code starts here
;
START:
	PUSH	IY		; Save IY or face consequences

	LD	HL,WELCOM
	CALL	P.CSTR

	; Print all ASCII characters
	; For B:=1 to 3
	LD	B,1
@BLOOP:
	; For C:=0 to 31
	LD	C,0
@CLOOP:
	.VDU.C	' '
	; A=B*32+C
	LD	A,B
	SLA	A
	SLA	A
	SLA	A
	SLA	A
	SLA	A
	ADD	A,C
	;CALL	P.X8.S
	.VDU

	CALL	ISXDIGIT
	JR	NC,@NO
	LD	A,'+'
	JR	@F
@NO:	LD	A,'-'
@@:	.VDU

	; Next C
	INC	C
	LD	A,31
	CP	C
	JR	NC,@CLOOP
	CALL	P.NL

	; Next B
	INC	B
	LD	A,3
	CP	B
	JR	NC,@BLOOP

	CALL	P.NL

	LD	HL,0		; return success
	POP	IY
	RET

;--------------------------------------------------------------
; PROGRAM DATA
WELCOM:	.ASCII	"Testing ISXDIGIT\r\n"
	.ASCII	"$Id: isxdigit.asm,v 1.1 2023-08-08 18:14:27+02 radek Exp $\r\n"
	.ASCIZ	"Testing implementation of ISXDIGIT\r\n"

;--------------------------------------------------------------
;
P.X8.S:
	PUSH	AF
	CALL	P.X8
	POP	AF
	RET


;--------------------------------------------------------------
; INCLUDE needed libraries
;
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "/lib/asm/p.nl.inc"
	.INCLUDE "/lib/asm/p.x8.inc"
	.INCLUDE "/lib/asm/p.x4.inc"
	.INCLUDE "../src/isxdigit.inc"
