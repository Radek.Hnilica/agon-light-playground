# Test programs for ASMLIB
Here I'm writing program which test ASMLIB subroutines.  This way I want
to limit problems with non working subroutines.


The tests:
- `p.cstr.asm`		-- success
- `p.nl.asm`		-- success
- `p.nl.s.asm`		-- success
- `p.x4.asm`		-- success
- `p.x8.asm`		-- success
- `p.x16.asm`		-- success
- `p.x24.asm`		-- success
- `p.mem.asm`		-- success
- `p.mem8.asm`		-- success
- `p.memr.asm`		-- success
- `u24toub8.asm`	-- success
- `ub8tou24.asm`	-- success
- `isxdigit.asm`	-- success
- `toupper.asm`		-- success
