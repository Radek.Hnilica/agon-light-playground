; $Id: p.nl.asm,v new date time developer $
; $Source: path/to/the/file $
; Testing subroutine P.NL
;---------------------------------------------------------------
;
;BUILD & INSTALL:
;	ez80asm p.nl.asm /mos/test.p.nl.bin -lv
;
;RUN:
;	test.p.nl
;
	;.INCLUDE "/lib/asm/macros.inc"
	.INCLUDE "../macros.inc"


	.FILLBYTE 0
	.ASSUME	ADL=1		; eZ80 ADL
	.ORG	$0B0000		; where the MOS commands lives
	JR	START		; where the real code lives

	; MOS program header
	.ALIGN	$40		; Header is at offset 64 ($40)
	.DB	"MOS",0,1	; MOS, version 0, 1=ADL mode


;--------------------------------------------------------------
; START - the real code starts here
;
START:
	PUSH	IY		; Save IY or face consequences

	LD	HL,WELCOM
	CALL	P.CSTR

	.VDU.C	'#'
	CALL	P.NL
	CALL	P.NL

	LD	HL,0		; return success
	POP	IY
	RET

;--------------------------------------------------------------
; PROGRAM DATA
WELCOM:	.ASCII	"Testing P.NL\r\n"
	.ASCIZ	"$Id: p.nl.asm,v new, 2023-08-07, 18:00 radek $\r\n"

;
;      |       |               |                               |       |
;        111111111122222222223333333333444444444455555555556666666666777777777
;23456789012345678901234567890123456789012345678901234567890123456789012345678
;

;--------------------------------------------------------------
; INCLUDE needed libraries
;
	.INCLUDE "../src/p.cstr.inc"
	.INCLUDE "../src/p.nl.inc"

