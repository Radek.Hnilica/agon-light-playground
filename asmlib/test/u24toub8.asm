; $Id: u24toub8.asm,v 1.1 2023-08-08 17:50:55+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/test/u24toub8.asm,v $
; Testing subroutine U24TOUB8
;---------------------------------------------------------------
;
;BUILD & INSTALL:
;	ez80asm u24toub8.asm /mos/test.u24toub8.bin -lv
;
;RUN:
;	test.u24toub8
;
	.INCLUDE "/lib/asm/macros.inc"


	.FILLBYTE 0
	.ASSUME	ADL=1		; eZ80 ADL
	.ORG	$0B0000		; where the MOS commands lives
	JR	START		; where the real code lives

	; MOS program header
	.ALIGN	$40		; Header is at offset 64 ($40)
	.DB	"MOS",0,1	; MOS, version 0, 1=ADL mode


;--------------------------------------------------------------
; START - the real code starts here
;
START:
	PUSH	IY		; Save IY or face consequences

	LD	HL,WELCOM
	CALL	P.CSTR

	; Trying print all possible variants is too much,
	; so I try only few.

	LD	HL,0
	CALL	U24TOUB8
	;TODO: print the 8 bytes whose address is in HL
	LD	B,8
	CALL	P.MEMR
	CALL	P.NL

	LD	HL,$A54C
	PUSH	HL
	CALL	P.X24
	POP	HL
	.VDU.C	'='
	CALL	U24TOUB8
	LD	B,8
	CALL	P.MEMR
	CALL	P.NL

	LD	HL,$123456
	PUSH	HL
	CALL	P.X24
	POP	HL
	.VDU.C	'='
	CALL	U24TOUB8
	LD	B,8
	CALL	P.MEMR
	CALL	P.NL

	LD	HL,$FEDCBA
	PUSH	HL
	CALL	P.X24
	POP	HL
	.VDU.C	'='
	CALL	U24TOUB8
	LD	B,8
	CALL	P.MEMR
	CALL	P.NL

	LD	HL,$FFFFFF
	PUSH	HL
	CALL	P.X24
	POP	HL
	.VDU.C	'='
	CALL	U24TOUB8
	LD	B,8
	CALL	P.MEMR
	CALL	P.NL

	CALL	P.NL

	LD	HL,0		; return success
	POP	IY
	RET

;--------------------------------------------------------------
; PROGRAM DATA
WELCOM:	.ASCII	"Testing U24TOUB8\r\n"
	.ASCII	"$Id: u24toub8.asm,v 1.1 2023-08-08 17:50:55+02 radek Exp $\r\n"
	.ASCIZ	"Testing implementation of U24TOUB8\r\n"

;--------------------------------------------------------------
; INCLUDE needed libraries
;
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "../src/p.nl.inc"
	.INCLUDE "../src/p.memr.inc"
	.INCLUDE "../src/p.x24.inc"
	.INCLUDE "../src/p.x16.inc"
	.INCLUDE "../src/p.x8.inc"
	.INCLUDE "../src/p.x4.inc"
	.INCLUDE "../src/u24toub8.inc"
