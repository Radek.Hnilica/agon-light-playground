; $Id: ub8tou24.asm,v 1.1 2023-08-08 17:51:33+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/test/ub8tou24.asm,v $
; Testing subroutine U24TOUB8
;---------------------------------------------------------------
;
;BUILD & INSTALL:
;	ez80asm ub8rou24.asm /mos/test.ub8tou24.bin -lv
;
;RUN:
;	test.ub8tou24
;
	.INCLUDE "/lib/asm/macros.inc"


	.FILLBYTE 0
	.ASSUME	ADL=1		; eZ80 ADL
	.ORG	$0B0000		; where the MOS commands lives
	JR	START		; where the real code lives

	; MOS program header
	.ALIGN	$40		; Header is at offset 64 ($40)
	.DB	"MOS",0,1	; MOS, version 0, 1=ADL mode


;--------------------------------------------------------------
; START - the real code starts here
;
START:
	PUSH	IY		; Save IY or face consequences

	LD	HL,WELCOM
	CALL	P.CSTR

	; Trying print all possible variants is too much,
	; so I try only few.

	LD	HL,BCD1
	CALL	UB8TOU24
	CALL	P.X24
	CALL	P.NL

	LD	HL,BCD2
	CALL	UB8TOU24
	CALL	P.X24
	CALL	P.NL

	LD	HL,0		; return success
	POP	IY
	RET

;--------------------------------------------------------------
; PROGRAM DATA
WELCOM:	.ASCII	"Testing UB8TOU24\r\n"
	.ASCII	"$Id: ub8tou24.asm,v 1.1 2023-08-08 17:51:33+02 radek Exp $\r\n"
	.ASCIZ	"Testing implementation of UB8TOU24\r\n"

BCD1:	.DB	05,01,02,07,07,07,06,01
BCD2:	.DB	06,04,00,03,09,01,01,00

;--------------------------------------------------------------
; INCLUDE needed libraries
;
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "../src/p.nl.inc"
	.INCLUDE "../src/p.memr.inc"
	.INCLUDE "../src/p.x24.inc"
	.INCLUDE "../src/p.x16.inc"
	.INCLUDE "../src/p.x8.inc"
	.INCLUDE "../src/p.x4.inc"
	.INCLUDE "../src/ub8tou24.inc"
; DEBUG
	.INCLUDE "../src/show.regs.inc"
	.INCLUDE "../src/p.mem8.inc"
