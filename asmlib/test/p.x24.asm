; $Id: p.x24.asm,v new date time developer $
; $Source: path/to/the/file $
; Testing subroutine P.X24
;---------------------------------------------------------------
;
;BUILD & INSTALL:
;	ez80asm p.x24.asm /mos/test.p.x24.bin -lv
;
;RUN:
;	test.p.x24
;
	.INCLUDE "/lib/asm/macros.inc"


	.FILLBYTE 0
	.ASSUME	ADL=1		; eZ80 ADL
	.ORG	$0B0000		; where the MOS commands lives
	JR	START		; where the real code lives

	; MOS program header
	.ALIGN	$40		; Header is at offset 64 ($40)
	.DB	"MOS",0,1	; MOS, version 0, 1=ADL mode


;--------------------------------------------------------------
; START - the real code starts here
;
START:
	PUSH	IY		; Save IY or face consequences

	LD	HL,WELCOM
	CALL	P.CSTR

	; Trying print all possible variants is too much,
	; so I try only few.



	LD	HL,0
	CALL	P.X24
	.VDU.C	' '

	LD	HL,1
	CALL	P.X24
	.VDU.C	' '

	LD	HL,$100
	CALL	P.X24
	.VDU.C	' '

	LD	HL,$10A
	CALL	P.X24
	.VDU.C	' '	

	LD	HL,$A54C
	CALL	P.X24
	.VDU.C	' '

	LD	HL,$BEEF
	CALL	P.X24
	.VDU.C	' '

	LD	HL,$FFA5
	CALL	P.X24
	.VDU.C	' '

	LD	HL,$FFFF
	CALL	P.X24
	.VDU.C	' '

	LD	HL,$10000
	CALL	P.X24
	.VDU.C	' '

	LD	HL,$123456
	CALL	P.X24
	.VDU.C	' '

	LD	HL,$FEDCBA
	CALL	P.X24
	.VDU.C	' '

	LD	HL,$FFFFFF
	CALL	P.X24

	CALL	P.NL

	LD	HL,0		; return success
	POP	IY
	RET

;--------------------------------------------------------------
; PROGRAM DATA
WELCOM:	.ASCII	"Testing P.X24\r\n"
	.ASCII	"$Id: p.x24.asm,v new, 2023-08-07, 18:00 radek $\r\n"
	.ASCIZ	"Testing implementation of p.x24\r\n"

;
;      |       |               |                               |       |
;        111111111122222222223333333333444444444455555555556666666666777777777
;23456789012345678901234567890123456789012345678901234567890123456789012345678
;

;--------------------------------------------------------------
; INCLUDE needed libraries
;
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "../src/p.nl.inc"
	.INCLUDE "../src/p.x24.inc"
	.INCLUDE "../src/p.x16.inc"
	.INCLUDE "../src/p.x8.inc"
	.INCLUDE "../src/p.x4.inc"
