; $Id: p.cstr.asm,v 1.1 2023-08-07 12:10:47+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/test/p.cstr.asm,v $
; Testing subroutine P.CSTR
;---------------------------------------------------------------
;
;BUILD & INSTALL:
;	ez80asm p.cstr.asm /mos/test.p.cstr.bin -lv
;
;RUN:
;	test.p.cstr
;
	;.INCLUDE "/lib/asm/macros.inc"
	.INCLUDE "../macros.inc"


	.FILLBYTE 0
	.ASSUME	ADL=1		; eZ80 ADL
	.ORG	$0B0000		; where the MOS commands lives
	JR	START		; where the real code lives

	; MOS program header
	.ALIGN	$40		; Header is at offset 64 ($40)
	.DB	"MOS",0,1	; MOS, version 0, 1=ADL mode


;--------------------------------------------------------------
; START - the real code starts here
;
START:
	PUSH	IY		; Save IY or face consequences

	LD	HL,WELCOM
	CALL	P.CSTR

	LD	HL,HELLO
	CALL	P.CSTR

	LD	HL,0		; return success
	POP	IY
	RET

;--------------------------------------------------------------
; PROGRAM DATA
WELCOM:	.ASCII	"Testing P.NL\r\n"
	.ASCIZ	"$Id: p.cstr.asm,v 1.1 2023-08-07 12:10:47+02 radek Exp $\r\n"
HELLO:	.ASCIZ	"Hello programmer!\r\n"

;--------------------------------------------------------------
; INCLUDE needed libraries
;
	.INCLUDE "../src/p.cstr.inc"

