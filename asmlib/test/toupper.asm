; $Id: toupper.asm,v 1.1 2023-08-08 18:14:43+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/test/toupper.asm,v $
; Testing subroutine TOUPPER
;---------------------------------------------------------------
;
;BUILD & INSTALL:
;	ez80asm toupper.asm /mos/test.toupper.bin -lv
;
;RUN:
;	test.toupper
;
	.INCLUDE "/lib/asm/macros.inc"

	.FILLBYTE 0
	.ASSUME	ADL=1		; eZ80 ADL
	.ORG	$0B0000		; where the MOS commands lives
	JR	START		; where the real code lives

	; MOS program header
	.ALIGN	$40		; Header is at offset 64 ($40)
	.DB	"MOS",0,1	; MOS, version 0, 1=ADL mode


;--------------------------------------------------------------
; START - the real code starts here
;
START:
@ENTR:	PUSH	IY		; Save IY or face consequences

	LD	HL,WELCOM
	CALL	P.CSTR

	; Printing all the printable ASCII characters from 32
	; to 126 (127 is DEL)
	; The characters are printed in three rows, 32
	; characters per line to have lowercas and uppercase
	; close.
	;
	; Print all ASCII characters
	; For B:=1 to 3		// rows 1 to 3
	LD	B,1
@BLOOP:
	; For C:=0 to 31	// columns from 0 to 31
	LD	C,0
@CLOOP:
	.VDU.C	' '		; separate from previous print
	; A=B*32+C
	LD	A,B
	SLA	A
	SLA	A
	SLA	A
	SLA	A
	SLA	A
	ADD	A,C
	CP	127		; If DEL, terminate
	JR	Z,@TERM		;+the program

	;CALL	P.X8.S
	.VDU			; print the ASCII character
	CALL	TOUPPER		; change to upper case
	.VDU			; print again

	; Next C
	INC	C
	LD	A,31
	CP	C
	JR	NC,@CLOOP
	CALL	P.NL

	; Next B
	INC	B
	LD	A,3
	CP	B
	JR	NC,@BLOOP

@TERM:	CALL	P.NL

	LD	HL,0		; return success
@LEAV:	POP	IY
	RET

;--------------------------------------------------------------
; PROGRAM DATA
WELCOM:	.ASCII	"Testing TOUPPER\r\n"
	.ASCII	"$Id: toupper.asm,v 1.1 2023-08-08 18:14:43+02 radek Exp $\r\n"
	.ASCIZ	"Testing implementation of TOUPPER\r\n"

;--------------------------------------------------------------
;
P.X8.S:
	PUSH	AF
	CALL	P.X8
	POP	AF
	RET


;--------------------------------------------------------------
; INCLUDE needed libraries
;
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "/lib/asm/p.nl.inc"
	.INCLUDE "/lib/asm/p.x8.inc"
	.INCLUDE "/lib/asm/p.x4.inc"
	.INCLUDE "../src/toupper.inc"
