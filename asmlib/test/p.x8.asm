; $Id: p.x8.asm,v new date time developer $
; $Source: path/to/the/file $
; Testing subroutine P.X8
;---------------------------------------------------------------
;
;BUILD & INSTALL:
;	ez80asm p.x8.asm /mos/test.p.x8.bin -lv
;
;RUN:
;	test.p.x8
;
	.INCLUDE "/lib/asm/macros.inc"


	.FILLBYTE 0
	.ASSUME	ADL=1		; eZ80 ADL
	.ORG	$0B0000		; where the MOS commands lives
	JR	START		; where the real code lives

	; MOS program header
	.ALIGN	$40		; Header is at offset 64 ($40)
	.DB	"MOS",0,1	; MOS, version 0, 1=ADL mode


;--------------------------------------------------------------
; START - the real code starts here
;
START:
	PUSH	IY		; Save IY or face consequences

	LD	HL,WELCOM
	CALL	P.CSTR

	; Print all 256 hexadecimal numbers from $00 to $FF
	XOR	A,A		; Zero

	; For B:=0 TO 15
	LD	B,0
@ROW_LOOP:
	
	LD	A,B
	CALL	P.X4

	; For C:=0 TO 15
	LD	C,0
@COL_LOOP:
	.VDU.C	' '
	; Construct byte to print.  A:= B<<4 || C
	LD	A,B
	SLA	A
	SLA	A
	SLA	A
	SLA	A
	OR	C
	
	CALL	P.X8

	INC	C
	LD	A,16
	CP	C
	JR	NZ,@COL_LOOP

	CALL	P.NL
	INC	B		; Bump row xounter
	LD	A,16
	CP	B
	JR	NZ,@ROW_LOOP

	CALL	P.NL

	LD	HL,0		; return success
	POP	IY
	RET

;--------------------------------------------------------------
; PROGRAM DATA
WELCOM:	.ASCII	"Testing P.X4\r\n"
	.ASCII	"$Id: p.x4.asm,v new, 2023-08-07, 18:00 radek $\r\n"
	.ASCIZ	"Testing implementation of p.x4\r\n"

;
;      |       |               |                               |       |
;        111111111122222222223333333333444444444455555555556666666666777777777
;23456789012345678901234567890123456789012345678901234567890123456789012345678
;

;--------------------------------------------------------------
; INCLUDE needed libraries
;
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "../src/p.nl.inc"
	.INCLUDE "../src/p.x8.inc"
	.INCLUDE "../src/p.x4.inc"
