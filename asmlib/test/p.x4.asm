; $Id: p.x4.asm,v new date time developer $
; $Source: path/to/the/file $
; Testing subroutine P.X4
;---------------------------------------------------------------
;
;BUILD & INSTALL:
;	ez80asm p.x4.asm /mos/test.p.x4.bin -lv
;
;RUN:
;	test-p.x4
;
	;.INCLUDE "/lib/asm/macros.inc"
	.INCLUDE "../macros.inc"


	.FILLBYTE 0
	.ASSUME	ADL=1		; eZ80 ADL
	.ORG	$0B0000		; where the MOS commands lives
	JR	START		; where the real code lives

	; MOS program header
	.ALIGN	$40		; Header is at offset 64 ($40)
	.DB	"MOS",0,1	; MOS, version 0, 1=ADL mode


;--------------------------------------------------------------
; START - the real code starts here
;
START:
	PUSH	IY		; Save IY or face consequences

	LD	HL,WELCOM
	CALL	P.CSTR

	; Print all 16 hexadecimal dogits from 0 to F
	LD	B,16
	LD	C,0

@@:	LD	A,C
	CALL	P.X4
	.VDU.C	' '
	INC	C
	DJNZ	@B
	CALL	P.NL

	LD	HL,0		; return success
	POP	IY
	RET

;--------------------------------------------------------------
; PROGRAM DATA
WELCOM:	.ASCII	"Testing P.X4\r\n"
	.ASCII	"$Id: p.x4.asm,v new, 2023-08-07, 18:00 radek $\r\n"
	.ASCIZ	"Testing implementation of p.x4\r\n"

;
;      |       |               |                               |       |
;        111111111122222222223333333333444444444455555555556666666666777777777
;23456789012345678901234567890123456789012345678901234567890123456789012345678
;

;--------------------------------------------------------------
; INCLUDE needed libraries
;
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "/lib/asm/p.nl.inc"
	.INCLUDE "/lib/asm/p.x4.inc"
