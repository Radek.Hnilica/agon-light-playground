; $Id: p.bcd.asm,v 1.1 2023-08-04 17:47:44+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/p.bcd.asm,v $
; Unsigned Int 24 to BCD
;---------------------------------------------------------------
; P.BCD - print number in BCD
; Remeber the number is in special on byte per digit format.
; IN:	HL - memory, digits are in low endian
;	B - number of digits
; OUT:	screen
; DESTROY: AF,B,DE,HL
; DEPENDS: -
; NOTE:
;
P.BCD:	LD	DE,0		; DE:=B-1
	LD	E,B		;+
	DEC	DE		;+
	ADD	HL,DE		; HL:= HL+B-1; point to MS digit
	; HL points most significant digit, B=8
@SKIP:	LD	A,(HL)
	OR	A,A
	JR	NZ,@PRINT

	DEC	HL		; next digit
	DJNZ	@SKIP		; we have wight of digits
	LD	A,' '		; all digits are 0
	RST	$10
	RET

@PRINT:	LD	A,(HL)		; get BCD
	OR	A,'0'		; make character
	RST.L	$10		; VDU
	DEC	HL		; next digit
	DJNZ	@PRINT		;+
	RET
