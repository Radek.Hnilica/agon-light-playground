#!/bin/bash
# $Id$
# $Source$
# Pull source, listing, symbols, and binary from sd card.
# Copyright (c) 2023 Radek Hnilica

declare SD=/media/radek/AEDE-11B1

cp -v $SD/src/sysvars/sysvars.{asm,lst,symbols,bin} .
cp -v $SD/src/sysvars/mos_api.inc .
