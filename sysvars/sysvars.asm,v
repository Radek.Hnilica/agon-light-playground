head	0.2;
access;
symbols;
locks
	radek:0.2; strict;
comment	@;; @;


0.2
date	2023.07.16.15.01.53;	author radek;	state Exp;
branches;
next	0.1;

0.1
date	2023.07.16.12.42.44;	author radek;	state Exp;
branches;
next	;


desc
@Display MOS system variables
@


0.2
log
@Using fixed version of mos_api.inc.  Added few other varaibles.
@
text
@; $Id: sysvars.asm,v 0.1 2023-07-16 14:42:44+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/sysvars/sysvars.asm,v $
; Display actual MOS system variable values
; Copyright (c) 2023 Radek Hnilica
;
; Build: ez80asm sysvars.asm sysvars.bin -l -s
; Install:
;	delete /mos/sysvars.bin
;	copy sysvars.bin /mos/sysvars.bin
;
		; Fixed inc file from MOS 1.03
		.INCLUDE "mos_api.inc"

		.fillbyte $00
		.assume	adl=0		; For legacy Z80 mode
		.org	$0000		; code address for MOS command
		jp	start

		; RST vectors

		; MOS call
		.ALIGN	8
RST_08:		RST.LIS	8
		RET
		; Because I use trampolines for MOS calls, I define
		; own macro for this
		.MACRO	MOS	function
		LD	A,function
		RST	$08
		.ENDMACRO

		; VDU char
		.ALIGN	8
RST_10:		RST.LIS	$10
		RET
		; Macro for VDU char trampoline
		.MACRO	VDU_CH
		RST	$10
		.ENDMACRO
		.MACRO	VDU	char
		LD	A,char
		RST	$10
		.ENDMACRO

		; VDU string
		.align	8
rst_18:		rst.lis $18
		ret

		.align	8
rst_20:		rst.lis	$20
		ret

		.align	8
rst_28:		rst.lis	$28
		ret

		.align	8
rst_30:		rst.lis	$30
		ret

		; NMI
		.align	8
		ei
		reti


; MOS program header
		; there is space for code, data up to
		.align	$40
		.db	"MOS"		; MOS command magic number
		.db	0		; header version
		.db	0		; Z80 binary

start:
		ld	a,mb
		cp	$0B
		jr	nz,start1

		ld	ix,0
		add	ix,sp
		push.lis ix
		ld	sp,$7FFE


start1:		push.l	af		; push Mbase register onto SPL stack
		ei
		call	main

		pop.l	af
		cp	$0B
		jr	nz,start2
		pop.lis	ix
		ld.sis	sp,ix

start2:
		ld	hl,0
		ret.l



;
; Main program start here
main:

		LD	HL,MSG_HELLO
		CALL	PRSTR

		.MOS	mos_sysvars

		; Print memory pointer to sysvars
		LD	HL,MSG_MEMPTR1
		CALL	PRSTR
		PUSH.L	IX		; IX->HL in ADL
		POP.L	HL		;+
		CALL	PRHEX24
		LD	HL,MSG_MEMPTR2
		CALL	PRSTR

		; Print time in ticks
		LD	HL,MSG_TIME
		CALL	PRSTR
		PUSH.L	IX
		POP.L	HL
		LD	BC,3
		ADD	HL,BC
		LD	B,4
@@@@:		LD	A,(HL)
		CALL	PRHEX8
		DEC	HL
		DJNZ	@@B
		CALL	PRCRLF

		; Print vpd_pflags
		LD	HL,MSG_VPD_PFLAGS
		CALL	PRSTR
		LD.L	A,(IX+$4)
		CALL	PRHEX8
		CALL	PRCRLF


		; Print sysvars_mode
		; This system variable is not defined in MOS 1.03

		; Print Screen width x height in pixels
		LD	HL,MSG_SCR_SIZE_PX
		CALL	PRSTR
		LD	A,'$'
		.VDU_CH
		LD	L,(IX+sysvar_scrWidth)
		LD	H,(IX+sysvar_scrWidth+1)
		CALL	PRHEX16
		CALL	PRSP
		LD	A,'x'
		.VDU_CH
		CALL	PRSP
		VDU	'$'
		LD	HL,(IX+sysvar_scrHeight)
		CALL	PRHEX16
		CALL	PRSP
		LD	A,'p'
		.VDU_CH
		LD	A,'x'
		.VDU_CH
		CALL	PRCRLF

		; Screen size in Columns and Rows
		LD	HL,MSG_SCR_SIZE_CH
		CALL	PRSTR
		LD	A,(IX+sysvar_scrCols)
		CALL	PRHEX8
		LD	HL,MSG_SCR_SIZE_CH_COL
		CALL	PRSTR
		LD	A,(IX+sysvar_scrRows)
		CALL	PRHEX8
		LD	HL,MSG_SCR_SIZE_CH_ROW
		CALL	PRSTR
		CALL	PRCRLF

		LD	HL,MSG_DONE
		CALL	PRSTR
		RET
MSG_DONE:	.ASCIZ	"DONE\r\n"

		;-----------------------------------------------------
		CALL	PRCRLF

		
		RET

		; Strings
MSG_HELLO:	.ASCIZ	"SYSVARS 0.1.4 (c) 2023 Radek Hnilica\r\n"
MSG_MEMPTR1:	.ASCIZ	"System Variables ($"
MSG_MEMPTR2:	.ASCIZ	"):\r\n"
MSG_TIME:	.ASCIZ	"time:\t\t$"
MSG_VPD_PFLAGS:	.ASCIZ	"vpd_pfalgs:\t"
MSG_SCR_SIZE_PX:	.ASCIZ	"screen:\t\t"
MSG_SCR_SIZE_CH:	.ASCIZ "screen:\t\t$"
MSG_SCR_SIZE_CH_COL:	.ASCIZ	" cols x $"
MSG_SCR_SIZE_CH_ROW:	.ASCIZ	" rows"

MSG_MB:		.ASCIZ	"MB: "
MSG_SYSVARS:	.ASCIZ	"SYSVARS(IX): "


; Print zero terminated string
; HL-points to string
PRSTR:		LD	A,(HL)		; get string character
		OR	A		; update Z flag
		RET	Z		; termination 0 found
		.VDU_CH			; VDU A
		INC	HL		; next char in string
		JR	PRSTR

; Data


		; Print \r\n
PRCRLF:		PUSH	AF		; Save register A before using
		LD	A,$0D
		.VDU_CH
		LD	A,$0A
		.VDU_CH
		POP	AF		; Restore used registers	
		RET

PRSP:		; Print SP
		PUSH	AF
		LD	A,32
		.VDU_CH
		POP	AF
		RET


		; Print a 24-bit hexadecimal number
		; HLU: number to print
PRHEX24:	PUSH.LIL	HL
		LD.LIL		HL,2
		ADD.LIL		HL,SP
		LD.LIL		A,(HL)
		POP.LIL		HL
		CALL		PRHEX8
		; pass through


		; Print a 16-bit hexadecimal number
		; HL: number to print
PRHEX16:	LD	A,H
		CALL	PRHEX8
		LD	A,L
		; pass through

		; Print an 8-bit hexadecimal number
		; A: number to print
PRHEX8:		PUSH	AF
		RRA
		RRA
		RRA
		RRA
		CALL	@@F
		POP	AF
		; pass through
@@@@:		AND	$0F
		ADD	A,$90
		DAA
		ADC	A,$40
		DAA
		.VDU_CH		; VDU char
		RET

		; Print stack content for debugging
PRSTACK:	LD	(SAVE_HL),HL
		LD	HL,MSG_SP
		CALL	PRSTR

		LD	HL,0		; Get stack into HL	
		ADD	HL,SP		;+
		CALL	PRHEX16
		LD	A,':'
		.VDU_CH

		LD	B,6
@@@@:		CALL	PRSP
		LD	A,(HL)
		INC	HL
		CALL	PRHEX8
		DJNZ	@@B


		CALL	PRCRLF
		LD	HL,(SAVE_HL)
		RET

MSG_STACK:	ASCIZ	"STACK:"
MSG_SP:		ASCIZ	"SP:"
SAVE_HL:	DW	0
@


0.1
log
@Display MOS system variables
@
text
@d1 2
a2 2
; $Id: sysvars.asm,v 1.1 2023-07-13 20:14:05+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/hello/hello.asm,v $
d11 3
d22 9
a30 3
		.align	8
rst_08:		rst.lis	8
		ret
d33 11
a43 3
		.align	8
rst_10:		rst.lis	$10
		ret
d108 2
a109 2
		LD	A,$08		; mos_sysvars
		RST	8		;+MOSCALL
d134 2
a135 1
		LD	HL,MSG_DONE
d137 3
a139 2
		RET
MSG_DONE:	.ASCIZ	"DONE\r\n"
a140 1
		;-----------------------------------------------------
d142 2
d145 11
a155 3
		PUSH.L	IX		; LD UHL,UXL
		POP.L	HL		;+
		CALL	PRHEX24
d157 2
a158 2
		PUSH	IX		; sysvars are in 16-bit space
		POP	HL
a159 9
		CALL	PRCRLF
		
		; Print memory block pointed to by IX
		;CALL	PRSTACK
		PUSH	IX
		POP	HL
		LD	B,$29
@@@@:		LD	A,(HL)
		CALL	PRHEX8
d161 4
a164 2
		INC	HL
		DJNZ	@@B
d167 8
a174 9
		CALL	PRSTACK

		PUSH.L	IX
		POP.L	HL
		
		LD	A,$08
		RST	8

		LD.L	A,(IX+$1)	; sysvar_time, b2
d176 2
d180 1
a180 1
		LD	HL,MSG_MB
d182 4
a185 5
		LD	A,(IY+8)
		CALL	PRHEX8
		CALL	PRSP
		LD	A,MB
		CALL	PRHEX8
d192 1
a192 1
MSG_HELLO:	.ASCIZ	"SYSVARS 0.1 (c) 2023 Radek Hnilica\r\n"
d196 5
d211 1
a211 1
		RST.LIS	10h		; VDU A
d219 1
a219 1
PRCRLF:		PUSH	AF
d221 1
a221 1
		RST	$10
d223 2
a224 2
		RST	$10
		POP	AF
d230 1
a230 1
		RST	$10
d268 1
a268 1
		RST	$10	; VDU char
d280 1
a280 1
		RST	$10
@
