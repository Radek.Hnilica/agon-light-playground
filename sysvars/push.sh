#!/bin/bash
# $Id$
# $Source$
# Push RCS versioned source back to sd card
# Copyright (c) 2023 Radek Hnilica

declare SD=/media/radek/AEDE-11B1

cp -v sysvars.asm sysvars.asm,v $SD/src/sysvars/
