; $Id: sysvars.asm,v 0.2 2023-07-16 17:01:53+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/sysvars/sysvars.asm,v $
; Display actual MOS system variable values
; Copyright (c) 2023 Radek Hnilica
;
; Build using ez80asm 0.101
; Build: ez80asm sysvars.asm sysvars.bin -l -s
; Install:
;	delete /mos/sysvars.bin
;	copy sysvars.bin /mos/sysvars.bin
;
		; Fixed inc file from MOS 1.03
		.INCLUDE "mos_api.inc"

		.fillbyte $00
		.assume	adl=0		; For legacy Z80 mode
		.org	$0000		; code address for MOS command
		jp	start

		; RST vectors

		; MOS call
		.ALIGN	8
RST_08:		RST.LIS	8
		RET
		; Because I use trampolines for MOS calls, I define
		; own macro for this
		.MACRO	MOS	function
		LD	A,function
		RST	$08
		.ENDMACRO

		; VDU char
		.ALIGN	8
RST_10:		RST.LIS	$10
		RET
		; Macro for VDU char trampoline
		.MACRO	VDU_CH
		RST	$10
		.ENDMACRO
		.MACRO	VDU	char
		LD	A,char
		RST	$10
		.ENDMACRO

		; VDU string
		.align	8
rst_18:		rst.lis $18
		ret

		.align	8
rst_20:		rst.lis	$20
		ret

		.align	8
rst_28:		rst.lis	$28
		ret

		.align	8
rst_30:		rst.lis	$30
		ret

		; NMI
		.align	8
		ei
		reti


; MOS program header
		; there is space for code, data up to
		.align	$40
		.db	"MOS"		; MOS command magic number
		.db	0		; header version
		.db	0		; Z80 binary

start:
		ld	a,mb
		cp	$0B
		jr	nz,start1

		ld	ix,0
		add	ix,sp
		push.lis ix
		ld	sp,$7FFE


start1:		push.l	af		; push Mbase register onto SPL stack
		ei
		call	main

		pop.l	af
		cp	$0B
		jr	nz,start2
		pop.lis	ix
		ld.sis	sp,ix

start2:
		ld	hl,0
		ret.l



;
; Main program start here
main:

		LD	HL,MSG_HELLO
		CALL	PRSTR

		.MOS	mos_sysvars

		; Print memory pointer to sysvars
		LD	HL,MSG_MEMPTR1
		CALL	PRSTR
		PUSH.L	IX		; IX->HL in ADL
		POP.L	HL		;+
		CALL	PRHEX24
		LD	HL,MSG_MEMPTR2
		CALL	PRSTR

		; Print time in ticks
		LD	HL,MSG_TIME
		CALL	PRSTR
		PUSH.L	IX
		POP.L	HL
		LD	BC,3
		ADD	HL,BC
		LD	B,4
@@:		LD	A,(HL)
		CALL	PRHEX8
		DEC	HL
		DJNZ	@B
		CALL	PRCRLF

		; Print vpd_pflags
		LD	HL,MSG_VPD_PFLAGS
		CALL	PRSTR
		LD.L	A,(IX+$4)
		CALL	PRHEX8
		CALL	PRCRLF


		; Print sysvars_mode
		; This system variable is not defined in MOS 1.03

		; Print Screen width x height in pixels
		LD	HL,MSG_SCR_SIZE_PX
		CALL	PRSTR
		LD	A,'$'
		.VDU_CH
		LD	L,(IX+sysvar_scrWidth)
		LD	H,(IX+sysvar_scrWidth+1)
		CALL	PRHEX16
		CALL	PRSP
		LD	A,'x'
		.VDU_CH
		CALL	PRSP
		VDU	'$'
		LD	HL,(IX+sysvar_scrHeight)
		CALL	PRHEX16
		CALL	PRSP
		LD	A,'p'
		.VDU_CH
		LD	A,'x'
		.VDU_CH
		CALL	PRCRLF

		; Screen size in Columns and Rows
		LD	HL,MSG_SCR_SIZE_CH
		CALL	PRSTR
		LD	A,(IX+sysvar_scrCols)
		CALL	PRHEX8
		LD	HL,MSG_SCR_SIZE_CH_COL
		CALL	PRSTR
		LD	A,(IX+sysvar_scrRows)
		CALL	PRHEX8
		LD	HL,MSG_SCR_SIZE_CH_ROW
		CALL	PRSTR
		CALL	PRCRLF

		LD	HL,MSG_DONE
		CALL	PRSTR
		RET
MSG_DONE:	.ASCIZ	"DONE\r\n"

		;-----------------------------------------------------
		CALL	PRCRLF

		
		RET

		; Strings
MSG_HELLO:	.ASCIZ	"SYSVARS $Revision: 0.2 $ (c) 2023 Radek Hnilica\r\n"
MSG_MEMPTR1:	.ASCIZ	"System Variables ($"
MSG_MEMPTR2:	.ASCIZ	"):\r\n"
MSG_TIME:	.ASCIZ	"time:\t\t$"
MSG_VPD_PFLAGS:	.ASCIZ	"vpd_pfalgs:\t"
MSG_SCR_SIZE_PX:	.ASCIZ	"screen:\t\t"
MSG_SCR_SIZE_CH:	.ASCIZ "screen:\t\t$"
MSG_SCR_SIZE_CH_COL:	.ASCIZ	" cols x $"
MSG_SCR_SIZE_CH_ROW:	.ASCIZ	" rows"

MSG_MB:		.ASCIZ	"MB: "
MSG_SYSVARS:	.ASCIZ	"SYSVARS(IX): "


; Print zero terminated string
; HL-points to string
PRSTR:		LD	A,(HL)		; get string character
		OR	A		; update Z flag
		RET	Z		; termination 0 found
		.VDU_CH			; VDU A
		INC	HL		; next char in string
		JR	PRSTR

; Data


		; Print \r\n
PRCRLF:		PUSH	AF		; Save register A before using
		LD	A,$0D
		.VDU_CH
		LD	A,$0A
		.VDU_CH
		POP	AF		; Restore used registers	
		RET

PRSP:		; Print SP
		PUSH	AF
		LD	A,32
		.VDU_CH
		POP	AF
		RET


		; Print a 24-bit hexadecimal number
		; HLU: number to print
PRHEX24:	PUSH.LIL	HL
		LD.LIL		HL,2
		ADD.LIL		HL,SP
		LD.LIL		A,(HL)
		POP.LIL		HL
		CALL		PRHEX8
		; pass through


		; Print a 16-bit hexadecimal number
		; HL: number to print
PRHEX16:	LD	A,H
		CALL	PRHEX8
		LD	A,L
		; pass through

		; Print an 8-bit hexadecimal number
		; A: number to print
PRHEX8:		PUSH	AF
		RRA
		RRA
		RRA
		RRA
		CALL	@F
		POP	AF
		; pass through
@@:		AND	$0F
		ADD	A,$90
		DAA
		ADC	A,$40
		DAA
		.VDU_CH		; VDU char
		RET

		; Print stack content for debugging
PRSTACK:	LD	(SAVE_HL),HL
		LD	HL,MSG_SP
		CALL	PRSTR

		LD	HL,0		; Get stack into HL	
		ADD	HL,SP		;+
		CALL	PRHEX16
		LD	A,':'
		.VDU_CH

		LD	B,6
@@:		CALL	PRSP
		LD	A,(HL)
		INC	HL
		CALL	PRHEX8
		DJNZ	@B


		CALL	PRCRLF
		LD	HL,(SAVE_HL)
		RET

MSG_STACK:	ASCIZ	"STACK:"
MSG_SP:		ASCIZ	"SP:"
SAVE_HL:	DW	0
