# CP/M
This is my attempt on putting CP/M on the Agon Light computer.
The difference against other implementations is that I plan to center the emulation in BDOS.
Such an solution will have advantage of directly mapping CP/M filesystem to FAT32 used by Agon Light.  Thus the files can be very esily copied in and out of the storage.  And also shared with other Agon Light programs.
