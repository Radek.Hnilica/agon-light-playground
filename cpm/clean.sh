#!/bin/bash
# $Id$
# $Source$
# Do some cleanup on SD card
# Copyright (c) 2023 Radek Hnilica

#set -xe

# Cleanup must be done on the original SD card!
declare -r SD=/media/radek/AEDE-11B1
declare -r PROJ=cpm

# Remove some working files I dont want to have on SD card,
# and it is too much manual work do it on Agon computer itself.
rm -v $SD/src/${PROJ}/*.{symbols,bak}
#rm -v $SD/src/${PROJ}/src/*.{symbols,bin,bak}
#rm -v $SD/src/${PROJ}/test/*.{symbols,bin,bak}
