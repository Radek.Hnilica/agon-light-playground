#!/bin/bash
# $Id$
# $Source$
# Push RCS versioned source back to sd card
# Copyright (c) 2023 Radek Hnilica

set -ex

# The push must be directly to SD card.
declare -r SD=/media/radek/AEDE-11B1
declare -r PROJ=cpm

cp -v *.{asm,'asm,v'}	$SD/src/${PROJ}/
