#!/bin/bash
# $Id: pull.sh,v new $
# $Source: playground/cpm $
# Pull source, listing, symbols, and binary from sd card or its backup.
# Copyright (c) 2023 Radek Hnilica

#set -xe
declare CPOPTS=-v

declare -r SD=/media/radek/AEDE-11B1
declare -r BKP=$HOME/st/repo/agonl2/backup
declare -r PROJ=cpm

cp $CPOPTS $BKP/src/${PROJ}/README.md .
cp $CPOPTS $BKP/src/${PROJ}/*.{asm,lst,bin,com} .
cp $CPOPTS $BKP/src/${PROJ}/test/*.{asm,inc,lst,com} test/
