; $Id: fdos.asm,v 0.8 2023-08-30 20:29:08+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/cpm/fdos.asm,v $
;==============================================================
; File:		fdos.asm
; Version:	0.0
; Author:	Radek Hnilica
; Copyright:	(c) 2023
; License:	GPL v3
;
; This is FDOS implementation of CPM for Agon Light computer.
; This is part of CP/M 8080/Z80 memory space.
; It contains BDOS followed by limited implementation of BIOS.
;
; BUILD:
;   ez80asm fdos.asm fdos.bin -lsv
;
; RUN: this code can't be run, it is used only by cpm.bin

	.INCLUDE "/lib/asm/mosapi.inc"

;mos_getkey:		.EQU	0
;mos_sysvars:		.EQU	8
;sysvar_keyascii:	.EQU	5


	.FILLBYTE 0
	.ASSUME	ADL=0	; Z80 memory mode
	.ORG $EE00


	; The BDOS starts with 6 bytes
	.DB	0	; OEM number, low byte
	.DB	22	; CP/M version, 16h = 2.2
	.DB	0	; OEM number, low byte
	.DB	0,0,0	; serial number, big-endian

; ----- BDOSE - BDOS entry point ------------------------------
; IN:	C = function, DE = parameter
	JP	BDOSE


SSIZE:	.EQU	24	; 24 level stack
REBOOT:	.EQU	$0000	; reboot system (CP/M)
IOLOC:	.EQU	$0003	; I/O byte location
BDOSA:	.EQU	$0006	; address field of JMP BDOS



; -------------------------------------------------------------
;
PERERR:	.DW	PERSUB	;permanent error subroutine
SELERR:	.DW	SELSUB	;select error subroutine
RODERR:	.DW	RODSUB	;ro disk error subroutine
ROFERR:	.DW	ROFSUB	;ro file error subroutine


; ----- Entry popints from MOS CPM ----------------------------
;
	JP	BDOSINI
	JP	RUNTPA

;--------------------------------------------------------------
; BDOS Entry Point.
;
; INPUT:
;	C - function number aka fn
;	E - 8-bit value
;	DE - 16-bit pointer or value
; OUTPUT:
;	A - result
;	L - same value as A
;
BDOSE:
	LD	HL,0		; Save user SP
	ADD	HL,SP		;+
	LD	(USERSP),HL	;+

	LD	SP,$FF80	; BDOS SP growns down from FF80
	LD	HL,BDOSRET	; Return address from BDOS,
	PUSH	HL		;+ push on the stack

	;CALL	DBG.BDOS.ARGS

	LD	A,C		; If the C is out of the
	CP	MAXCALL		;+BDOSTAB
	RET	NC		;THEN: return and don't call

	PUSH	DE		; Store the function argument

	LD	H,0		; Compute address in BDOSTAB:
	LD	L,C		;+ First we load fn into HL
	ADD	HL,HL		;+ Then we double it
	LD	DE,BDOSTAB	;+ Get address of the table
	ADD	HL,DE		;+ Sum together to get entry
	
	LD	A,(HL)		; Fetch the function address
	INC	HL		;+to HL
	LD	H,(HL)		;+
	LD	L,A		;+
	
	;PUSH	AF
	;.VDUC	'<'
	;CALL	P.X16		; Print the address of function
	;.VDUC	'>'
	;POP	AF

@PASS:	POP	DE		; Restore the function argument
	JP	(HL) 		; and pass to it.

	; Return from BDOS happens here
BDOSRET:
	LD	(SAVEHL),HL	; Save the function return value
	LD	HL,(USERSP)	; Restore SP
	LD	SP,HL		;+
	LD	HL,(SAVEHL)
	RET			; Back to user program in TPA

USERSP:	.DW	$0000
SAVEHL:	.DW	$0000

;--------------------------------------------------------------
; Trampoline to run code from CPM MOS command part of CP/M
;
RUNTPA:
	LD	(SAFESP),SP	; Save SP
	;.VDUC	'T'
	LD	SP,$DFFF	; Put Stack somewhere
	CALL	SHOW.STACK
	CALL	P.NL
	.VDUC	'K'

;	LD	B,10
;@@:	.MOS	getkey
;	CALL	P.X8
;	DJNZ	@B
;	CALL	P.NL	


	.VDUC	'R'
	CALL	$0100		; Run code in TPA
	;pass to BOOTF
;--------------------------------------------------------------
; P_TERMCPM:	; 0 - System Reset
;
; INPUT: C=0
; OUTPUT: n/a, does not return
BOOTF:
	.VDUC	'F'
	;CALL	SHOW.STACK
	;MOS	getkey		; Wait for keypress
	LD	SP,(SAFESP)	; Restore SP
	RET.L			; Back to CPM MOS command

SAFESP:	.BLKW	1


;
; NOTE:
; See the bdos.lst and get the address of RUNTPA.  This needs
; to be put into cpm.asm so the CPM command knows where RUNTPA
; is.

;--------------------------------------------------------------
; Initialize BDOS
; Code which initialize all needed stuff in Z80 memory to make
; BDOS working.
;
; NOT USED!
;
BDOSINI:
	LD	HL,$0080	; Initialize address of DMA
	LD	(DMA),HL	;+buffer.
	RET.L


;--------------------------------------------------------------
; C_READ - Console Input (BDOS 1)
; IN:	C=1
; OUT:	A - character
;
; Waits for and reads in character from console.  The character
; is echoad back to the display.
C_READ:
	CALL	CONIN		; Read char into A
	LD	C,A		; echo it on the console.
	CALL	CONOUT		;+
	LD	L,A		; Extend A into HL
	LD	H,0		;+
	RET			; return result in A and L

;--------------------------------------------------------------
; C_WRITE - Console Output, Function 2
; IN:	C=2; E - charcter to output
; OUT:	screen
;
C_WRITE:
	LD	C,E		; CONOUT expect char in C
	CALL	CONOUT		; write char in C
	RET


;--------------------------------------------------------------
; A_READ - Auxiliary (Reader) input (BDOS 3)
;
A_READ:
	LD	A,0
	LD	L,A
	RET

;--------------------------------------------------------------
;A_WRITE - Auxiliary (Punch) output (BDOS 4)
;IN:	C=4
;
A_WRITE:
	;*TODO:A_WRITE
	RET

;--------------------------------------------------------------
; L_WRITE - Printer output (BDOS 5)
;
L_WRITE:
	RET


;--------------------------------------------------------------
; C_RAWIO - Direct console I/O (BDOS 6)
;
C_RAWIO:
	RET


;--------------------------------------------------------------
; A_STATIN - Auxiliary Input status (BDOS 7)
;
A_STATIN:
	RET

;--------------------------------------------------------------
; A_STATOUT - Auxiliary Output status (BDOS 8)
;
A_STATOUT:
	RET

;--------------------------------------------------------------
; C_WRITESTR - Output String (BDOS 9)
; IN:	DE - adress of string terminated by '$' character
;
C_WRITESTR:
@LOOP:	LD	A,(DE)
	CP	'$'
	RET	Z
	CALL	CONOUT
	INC	DE
	JR	@LOOP

;--------------------------------------------------------------
; C_READSTR - Buffered cinsole input (BDOS 10)
; IN:	DE - address of buffer or zero (then DMA is used)
;
C_READSTR:
	LD	A,E
	OR	D
	JR	NZ,@F
	LD	DE,(DMA)
@@:
	;*TODO: Read string into ^DE

	RET

; Store the A register to ARET
;
STA$RET:
	LD	(ARET),A
	RET

PERSUB:
SELSUB:
RODSUB:
ROFSUB:
	NOP
	RET


;==============================================================
; Data

ARET:	.BLKB	1
DMA:	.DW	$0080	; Address of DMA buffer, default $80

;--- Table of vectors to functions ----------------------------
; Each vector is 16 bit address od subroutin implementing BDOS
; function.
;
BDOSTAB:
FUNCTAB: ;Alternate deprecated name
	.DW	BOOTF		;(0) P_TERMCPM
	.DW	C_READ		;(1) CONSOLEIN
	.DW	C_WRITE		;(2) CONSOLEOUT
	.DW	A_READ
	.DW	A_WRITE
	.DW	L_WRITE		;(5)
	.DW	C_RAWIO		;(6)
	.DW	A_STATIN	;(7)
	.DW	A_STATOUT	;(8)

MAXCALL:.EQU	8		; Highest implemented function

;==============================================================
;BDOS Calls to Implement

C_STAT:		;11 - Console status
S_BDOSVER:	;12 - Return version number
DRV_ALLRESET:	;13 - Reset discs
DRV_SET:	;14 - Select disc
F_OPEN:		;15 - Open file
F_CLOSE:	;16 - Close file
F_FIRST:	;17 - search for first
F_SNEXT:	;18 - search for next
F_DELETE:	;19 - delete file
F_READ:		; 20 - read next record
F_WRITE:	; 21 - write next record
F_MAKE:		; 22 - create file
F_RENAME:	; 23 - Rename file
DRV_LOGINVEC:	; 24 - Return bitmap of logged-in drives
DRV_GET:	; 25 - Return current drive
F_DMAOFF:	; 26 - Set DMA address
DRV_ALLOCVEC:	; 27 - Return address of allocation map
DRV_SETRO:	; 28 - Software write-protect current disc
DRV_ROVEC:	; 29 - Return bitmap of read-only drives
F_ATTRIB:	; 30 - set file attributes
DRV_DPB:	; 31 - get DPB address
F_USERNUM:	; 32 - get/set user number
F_READRAND:	; 33 - Random access read record
F_WRITERAND:	; 34 - Random access write record
F_SIZE:		; 35 - Compute file size
F_RANDREC:	; 36 - Update random access pointer
DRV_RESET:	; 37 - Selectively reset disc drives

F_WRITEZF:	; 40 - Write random with zero fill
S_SYSVAR:	;3.0 49 - Access the System Control Block
S_BIOS:		;3.0 50 - Use the BIOS





;==============================================================
;-----   B I O S   --------------------------------------------
; Some parts of BIOS needed
;
	.ALIGN	256	; Must begin at page boundary
	JP	BOOT	;-3: fn(0) Cold Boot
BIOSE:	JP	WBOOT	; 0: fn(1) Warm Boot
	JP	CONST	; 3: fn(2) Console Status
	JP	CONIN	; 6: fn(3) Console Input
	JP	CONOUT	; 9: fn(4) Console Output
	; Following calls are printer related and not implemented
	JP	LIST	;12: fn(5) Printer Output
	JP	PUNCH	;15: fn(6) Paper tape punch output
	JP	READER	;18: fn(7) Paper tape reader input
	; Rest of the calls are Floppy Disc oriented
	; CP/M 2 calls not implemented
	; CP/M 3 calls not implemented

; Cold Boot
BOOT:
; Warm Boot
WBOOT:
	JP	BOOTF		; SW Reset

; -------------------------------------------------------------
; BIOS funcion 2 CONST
; Return its status in A;  0 if no character is ready, 0FFh if
; one is.
;
CONST:
	;*FIXME: interogate sysvars
	;.MOS	sysvars
	;LD.L	A,(IX+sysvar_keyascii)
	; I try hard, however no success.  So I'm giving up,
	; and will try again when there is new version of MOS.
	; Newer that the actual on 1.03.
	
	LD	A,$FF		; Return char ready
	RET

; -------------------------------------------------------------
; BIOS call 3, CONIN
; Wait until the keyboard is ready to provide character, and
; return it in A.
;
CONIN:
	;CALL	DBG.BIOS.ARGS
@@:	.MOS	getkey	; kbd->A
	;PUSH	AF
	;PUSH	BC
	;LD	B,A
	;.VDUC	' '
	;LD	A,B
	;CALL	P.X8
	;POP	BC
	;POP	AF
	OR	A		; update flags (Z)
	JR	Z,@B	
	RET

; -------------------------------------------------------------
; BIOS function 4, CONOUT
; Write the character in C to the screen.
;
CONOUT:
	;CALL	DBG.BIOS.ARGS
	PUSH	AF
	LD	A,C
	.VDU
	POP	AF
	RET

LIST:
PUNCH:
READER:
	; Not Yet Implemented
	RET


;
;      |       |               |                               |       |
;        111111111122222222223333333333444444444455555555556666666666777777777
;23456789012345678901234567890123456789012345678901234567890123456789012345678
;

; Following functions I use for debugging purpose and once FDOS
; is in production will get removed.
;
; -------------------------------------------------------------
; Debug functions
;
SHOW.STACK:
	LD	HL,@MSG
	CALL	P.CSTR
	LD	HL,0
	ADD	HL,SP
	CALL	P.X16
	.VDUC	':'
	LD	B,8
	CALL	P.MEM8
	CALL	P.NL
	RET
	
@MSG:	ASCIZ	"SP: "

; -------------------------------------------------------------
; DBG.BDOS.ARGS - display on the screen BDOS call functiona and
; parameters.
; IN:	nothing
; OUT:	screen
; DESTROYS: hopefully nothing
;
DBG.BDOS.ARGS:
	PUSH	AF
	PUSH	BC
	PUSH	DE
	PUSH	HL

	LD	(@REGA),A
	LD	(@REGBC),BC
	LD	(@REGDE),DE
	LD	(@REGHL),HL

	CALL	P.NL
	.VDUC	'{'

	.VDUC	'B'
	.VDUC	'D'
	.VDUC	'O'
	.VDUC	'S'
	.VDUC	':'

	.VDUC	'A'
	.VDUC	'='
	LD	A,(@REGA)
	CALL	P.X8
	.VDUC	','

	.VDUC	'C'
	.VDUC	'='
	LD	A,(@REGBC)
	CALL	P.X8
	.VDUC	','

	.VDUC	'D'
	.VDUC	'E'
	.VDUC	'='
	LD	HL,(@REGDE)
	CALL	P.X16
	.VDUC	','

	.VDUC	'H'
	.VDUC	'L'
	.VDUC	'='
	LD	HL,(@REGHL)
	CALL	P.X16

	.VDUC	'}'

	POP	HL
	POP	DE
	POP	BC
	POP	AF
	RET

@REGA:	.BLKB	1
@REGBC:	.BLKW	1
@REGDE:	.BLKW	1
@REGHL:	.BLKW	1

DBG.BIOS.ARGS:
	PUSH	AF
	PUSH	BC
	PUSH	DE
	PUSH	HL

	LD	(@REGA),A
	LD	(@REGBC),BC
	LD	(@REGDE),DE
	LD	(@REGHL),HL

	.VDUC	'{'
	.VDUC	'B'
	.VDUC	'I'
	.VDUC	'O'
	.VDUC	'S'
	.VDUC	':'

	.VDUC	'A'
	.VDUC	'='
	LD	A,(@REGA)
	CALL	P.X8

	.VDUC	','
	.VDUC	'B'
	.VDUC	'C'
	.VDUC	'='

	LD	HL,(@REGBC)
	CALL	P.X16

	.VDUC	','
	.VDUC	'D'
	.VDUC	'E'
	.VDUC	'='
	LD	HL,(@REGDE)
	CALL	P.X16
	
	.VDUC	'}'

	POP	HL
	POP	DE
	POP	BC
	POP	AF
	RET

@REGA:	.BLKB	1
@REGBC:	.BLKW	1
@REGDE:	.BLKW	1
@REGHL:	.BLKW	1

; -------------------------------------------------------------
	.INCLUDE "/lib/asm/p.nl.inc"
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "/lib/asm/p.x16.inc"
	.INCLUDE "/lib/asm/p.x8.inc"
	.INCLUDE "/lib/asm/p.x4.inc"
	.INCLUDE "/lib/asm/p.mem8.inc"
