; $Id: cpm.asm,v 0.7 2023-08-30 18:24:45+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/cpm/cpm.asm,v $
;==============================================================
; File:
; Version:
; Date:
; Author:
; Copyright:	(c)
; License:	GPL v3
;
; This is CP/M emulator start command.  It is implemented as
; MOS command in ADL mode.
;
; COMPILE & INSTALL
;   ez80asm cpm.asm /mos/cpm.bin -lsv
;
; RUN:
;   cpm program.com
;
	.INCLUDE "/lib/asm/macros.inc"

; ----- Constants ---------------------------------------------
;
Z80MEM:	.EQU	$040000	; Z80 64KiB address space starts here 
TPA:	.EQU	$040100	; CP/M Transient Program Area
DMA:	.EQU	$040080	; CP/M DMA area

; ----- Symbol values from bdos.lst ---------------------------
;
FDOS:	.EQU	$EE00	; Where FDOS is loaded in Z80 memory
BDOSE:	.EQU	$EE06	; Fixed entry point to FDOS
BIOSE:	.EQU	$EF03	; Fixed entry to BIOS WBOOT (0)
BDOSINI:.EQU	$EE11	; Fixed entry
RUNTPA:	.EQU	$EE14	; Fixed entry
RUNZ80:	.EQU	$EE17	; NYI: Run Z80 code, PC:=HL

;
; ----- MOS API symbols not yet in macros.inc -----------------
;
mos_load:	.EQU	1
mos_getError:	.EQU	$0F


	.FILLBYTE 0
	.ASSUME	ADL=1
	.ORG	$0B0000
	JP	START


; ----- CPMVEC0 -----------------------------------------------
; Assembling CPMVEC0 to be injected into Z80 space.  Be sure
; you define BIOSE and BDOSE entry points at the top of this
; file
;
	.ASSUME	ADL=0		; Temporary go to Z80 mode
CPMVEC0:
	JP	BIOSE
	.DB	0
	.DB	0
	JP	BDOSE
	.ASSUME	ADL=1		; Back to ADL mode

; -------------------------------------------------------------
; MOS program header
	.ALIGN	64
	.DB	"MOS",0,1	; MOS, version 0, in ADL mode

; -------------------------------------------------------------
; Importat Program Static Data
;
FDOSFN:	.ASCIZ "/src/cpm/fdos.bin"	; FDOS file name

; Messages
;
WELCOM: .ASCII "CP/M Emulator 0.4.1, (c) 2023 Radek Hnilica\r\n"
	.ASCIZ "$Id: cpm.asm,v 0.7 2023-08-30 18:24:45+02 radek Exp $\r\n"
M.NOFDOS:	.ASCIZ "Can't load FDOS!\r\n"
M.Z80MEM:	.ASCIZ "Z80 0000:"
M.DMA:		.ASCIZ "DMA 0080:"
M.TPA:		.ASCIZ "TPA 0100:"

; -------------------------------------------------------------
; Program starting point
; Execution starts here
; This code is inspired by misc.asm:_exec16
;
START:
	PUSH	IY		; Save registers or else
	LD	(ARGP),HL	; Store argument pointer

	LD	HL,WELCOM
	CALL	P.CSTR
	CALL	P.NL

	; Show the CLI argument(s)
	.VDU.C	39
	LD	HL,(ARGP)
	CALL	P.CSTR
	.VDU.C	39
	CALL	P.NL

	;--- Initialize the memory of Z80 ---------------------
	; Initialize CP/M memory at address $0000
	LD	HL,CPMVEC0	; Copy CPMVEC0
	LD	DE,Z80MEM	;+into $0000..$0007 of Z80 mem,
	LD	BC,8		;+copy 8 bytes.
	LDIR			;+do it!

	;--- Load FDOS image into Z80 space -------------------
	; Address is $04EE00, File is fdos.bin
	;
	;*TODO: Inform user about loading FDOS.BIN
	LD	HL,FDOSFN
	LD	DE,Z80MEM+FDOS
	LD	BC,$1200
	MOS	mos_load
	LD	E,A
	OR	A,A
	JR	Z,@F		; Ok, Continue
	LD	HL,M.NOFDOS	; Err, Report and Exit
	CALL	P.CSTR
	JP	EXIT
@@:	;*TODO: Report FDOS.BIN loaded.

	;--- Load some testing program ------------------------
	; Address is $040100, File is test-00.bin
	; Load COM program into TPA
	;
	LD	HL,(ARGP)	; name of .COM to load
	LD	DE,TPA		; where
	LD	BC,32768	; max size
	MOS	mos_load
	LD	E,A		; Save the error code
	OR	A,A		; Check for error
	JR	Z,@F		;+ NO: continue
	; report error
	CALL	P.NL
	.VDU.C	'E'
	LD	A,E		;+ YES: Print the error
	CALL	P.X8		; Show status
	.VDU.C	' '
	LD	HL,ERRBUF
	LD	BC,ERRBUFL
	MOS	mos_getError	; Error is already in E
	LD	HL,ERRBUF
	CALL	P.CSTR
	.VDU.C	' '
	.VDU.C	'"'
	LD	HL,(ARGP)
	CALL	P.CSTR
	.VDU.C	'"'
	CALL	P.NL
	JP	EXIT
@@:
	; Do some debug printing
	LD	HL,M.Z80MEM
	CALL	P.CSTR
	LD	HL,Z80MEM
	LD	B,16
	CALL	P.MEM8
	CALL	P.NL

	LD	HL,M.DMA
	CALL	P.CSTR
	LD	HL,DMA		; Show first
	LD	B,16		;+16 bytes of
	CALL	P.MEM8		;+TPA
	CALL	P.NL

	LD	HL,M.TPA
	CALL	P.CSTR
	LD	HL,TPA		; Show first
	LD	B,16		;+16 bytes of
	CALL	P.MEM8		;+TPA
	CALL	P.NL

@@:

	; Run the testing program
	;TBD: Save registers
	PUSH	IY		; Save registers
	PUSH	AF		;+
	PUSH	BC		;+
	PUSH	DE		;+
	PUSH	HL		;+
	PUSH	IX		;+
	LD	A,MB		;+ preserve MB
	PUSH	AF		;+
	
	.VDU.C	'I'
	LD	A,$04		; Prepare MBASE for callig in
	LD	MB,A		;+Y80 space 
	;CALL.IS	BDOSINI		; Initialise the BDOS
	CALL.IS	RUNTPA		; Run the program in TPA
	
	.VDU.C	'C'
	MOS	getkey

CPMRET:	; Return from CP/M program
	
	; Restore registers
	POP	AF		;+ Restore the MBASE register
	LD	MB,A		;+
	POP	IX		;+
	POP	HL		;+
	POP	DE		;+
	POP	BC		;+
	POP	AF		;+
	POP	IY		;+
	;


	; WORK IN PROGRESS
	
	
EXIT:
	LD	HL,0		; Success
	POP	IY		; Restore registers
	RET

;--------------------------------------------------------------
; Data
;
TEST_COM:	.ASCIZ	"/src/cpm/td01.com"
ARGP:		.BLKP	1
ERRBUF:		.BLKB	200	; Buffer for error message
ERRBUFL:	.EQU	$-ERRBUF
;==============================================================
; Include asmlib modules
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "/lib/asm/p.nl.inc"
	.INCLUDE "/lib/asm/p.mem8.inc"
	.INCLUDE "/lib/asm/p.x8.inc"
	.INCLUDE "/lib/asm/p.x4.inc"
