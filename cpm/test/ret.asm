; CP/M program returning back to BDOS
;
; BUILD:
;   ez80asm ret.asm ret.com -l
;
; RUN:
;   cpm ret.com
;
; NOTE:
;   We do not use FDOS so it can be run without it.
;
	.FILLBYTE 0
	.ASSUME	ADL=0	; Z80 mode
	.ORG	$0100	; assembly into TPA

START:
	RET

