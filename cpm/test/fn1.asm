; CP/M program for testing BDOS call 1 (C_READ)
;
; BUILD:
;   ez80asm fn1.asm fn1.com -l
;
; RUN:
;   cpm fn1.com
;
;
;      |       |               |                               |       |
;        111111111122222222223333333333444444444455555555556666666666777777777
;23456789012345678901234567890123456789012345678901234567890123456789012345678
;

	.INCLUDE "BDOS.INC"

	.FILLBYTE 0
	.ASSUME	ADL=0	; Z80 mode
	.ORG	$0100	; assembly into CP/M TPA

START:
	LD	E,'>'
	.BDOS	C_WRITE
	.BDOS	C_READ	; kbd->A=L
	PUSH	AF
	LD	E,'='
	.BDOS	C_WRITE
	POP	AF
	LD	E,A
	.BDOS	C_WRITE

EXIT:	.BDOS	P_TERMCPM

