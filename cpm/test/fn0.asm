; CP/M program for testing BDOS call 0, P_TERMCPM
;
; BUILD:
;   ez80asm fn0.asm fn0.com -l
;
; RUN:
;   cpm fn0.com
;
	.FILLBYTE 0
	.ASSUME	ADL=0	; Z80 mode
	.ORG	$0100	; assembly into CP/M TPA

BDOS:	.EQU	$0005	; BDOS call

START:
	LD	C,0	; P_TERMCPM
	JP	BDOS
