; CP/M program for testing BDOS call 2 (C_WRITE)
;
; BUILD:
;   ez80asm fn2.asm fn2.com -l
;
; RUN:
;   cpm test/fn2.com
;
;
;      |       |               |                               |       |
;        111111111122222222223333333333444444444455555555556666666666777777777
;23456789012345678901234567890123456789012345678901234567890123456789012345678
;

	.INCLUDE "BDOS.INC"

	.FILLBYTE 0
	.ASSUME	ADL=0	; Z80 mode
	.ORG	$0100	; assembly into CP/M TPA


START:
	LD	E,'H'
	.BDOS	C_WRITE

	LD	E,'E'
	.BDOS	C_WRITE

	LD	E,'L'
	.BDOS	C_WRITE

	LD	E,'L'
	.BDOS	C_WRITE

	LD	E,'O'
	.BDOS	C_WRITE

	LD	E,10	; CR
	.BDOS	C_WRITE

	LD	E,13	; LF
	.BDOS	C_WRITE


EXIT:	.BDOS	P_TERMCPM

