; CP/M program for testing BIOS call 0, WBOOT
;
; BUILD:
;   ez80asm fn0.asm fn0.com -l
;
; RUN:
;   cpm fn0.com
;
; NOTE:
;
; BIOS call 0 behaves as software rest.  It can be initiated by
; - RST 0 instruction
; - JP 0 instruction
; - or directly calling BIOS WBOOT, however this is more
;   complicated
;
;      |       |               |                               |       |
;        111111111122222222223333333333444444444455555555556666666666777777777
;23456789012345678901234567890123456789012345678901234567890123456789012345678
;

	.FILLBYTE 0
	.ASSUME	ADL=0	; Z80 mode
	.ORG	$0100	; assembly into CP/M TPA

START:
	RST	0	; BDOS WBOOT

