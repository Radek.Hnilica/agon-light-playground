; CP/M program for testing BDOS call 1 (C_READ) and 2 (C_WRITE)
;
; BUILD:
;   ez80asm fn1-2.asm fn1-2.com -l
;
; RUN:
;   cpm fn1-2.com
;
;
;      |       |               |                               |       |
;        111111111122222222223333333333444444444455555555556666666666777777777
;23456789012345678901234567890123456789012345678901234567890123456789012345678
;

	.INCLUDE "BDOS.INC"

	.FILLBYTE 0
	.ASSUME	ADL=0	; Z80 mode
	.ORG	$0100	; assembly into CP/M TPA

START:
	.BDOS	C_READ	; kbd->A=L

	CP	'q'	; Did we got an 'q'
	JR	Z,EXIT	;+yes, terminate

	LD	E,A	; Echo the character
	.BDOS	C_WRITE	;+on the screen.

	JR	START	; Loop back

EXIT:	.BDOS	P_TERMCPM

