; CP/M program directly returning back to CPM.BIN
;
; BUILD:
;   ez80asm retl.asm retl.com -lsv
;
; RUN:
;   cpm retl.com
;
; NOTE:
;   We do not use FDOS so it can be run without it.
;
	.FILLBYTE 0
	.ASSUME	ADL=0	; Z80 mode
	.ORG	$0100	; assembly into TPA

START:
	RET.L

