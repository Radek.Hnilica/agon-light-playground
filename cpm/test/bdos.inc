; $Id: bdos.inc,v new.1 2023-08-28 radek $
; $Source$
; ============================================================
;
BDOS:		.EQU	$0005	; Entry address to BDOS for
				; COM programs.

		; Simplify calling BDOS functions with macro
		;
		.MACRO	BDOS	function
		LD	C,function
		CALL	BDOS
		.ENDMACRO

; CP/M interface, the BDOS function numbers.  For these
; functions:
; IN:	C=function, D or DE input paramter
; OUT:	A=L or HL
;
P_TERMCPM:	.EQU	0
C_READ:		.EQU	1	; kbd -> A=L
C_WRITE:	.EQU	2	; E -> screen

