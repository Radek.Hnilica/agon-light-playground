; $Id: break-ma.asm,v 1.2 2023-07-31 19:48:07+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/misc/break-ma.asm,v $
;
; Trying to break MOS by playing with registers
; Copyright (c) 2023 Radek Hnilica
;
; BUILD & INSTALL:
;	ez80asm break-ma.asm /mos/break.bin -lsv
;
; RUN:
;	break
;

;---------------------------------------------------------------
; Macros for MOS and VDU call
;
; NOTE: Remember, even if we are in ADL mode, we must mangle
;	RST instruction with '.L'.

	; MOS api, the reg. A contain service number
	.MACRO	MOS fn
	LD	A,fn
	RST.L	$10
	.ENDMACRO

	; Sending one character, in register A, to VDP.
	.MACRO	VDU
	RST.L	$10
	.ENDMACRO

	.MACRO	VDU.C	char
	LD	A,char
	RST.L	$10
	.ENDMACRO

;---------------------------------------------------------------

	.FILLBYTE $00
	.ASSUME	ADL=1	; eZ80 ASL

	.ORG	$0B0000	; Where the MOS commands live in
	JR	START	; Where the real code lives

	; There is space from start of the program and MOS
	; header.  You can use it as you wish for code,
	; variables, messages, ...
	; Just don't forget not to run into MOS header!


;
;        1111111111222222222233333333334444444444555555555566666
;234567890123456789012345678901234567890123456789012345678901234
;

;---------------------------------------------------------------
; MOS program header
	; there is space for code, data up to
	.ALIGN	$40	; Header is at offset $40
	.DB	"MOS",0,1	; MOS, version 0, 1-ADL mode

;---------------------------------------------------------------
; START - the real code of the application
; IN:	HL points at CLI
; OUT:	HL contains exit code
; NOTE:	Saves necessary registers
;
START:
	PUSH	IY		; Trying to avoid issues with IY

	; Show registers
	CALL	SHOW.REGS
	CALL	P.NL

	;EI	; Why?

	LD	IY,$020304	; Causes problem

	; On program/command start the HL contains address of
	; command line arguments.  So it's good idea to save it.
	LD	(ARGP),HL	; Save Argumeny Pointer to CLI
	

	LD	HL,$030405
	LD	DE,$020505
	LD	BC,$010609
	LD	A,$A5
	

	LD	HL,WELCOM
	CALL	P.CSTR
	CALL	P.NL

	LD	HL,MESSAGE
	CALL	P.CSTR
	CALL	P.NL

	LD	IX,$010203	; no problem
	LD	IY,$020304	; causes problem !!


	; Show registers
	CALL	SHOW.REGS	

EXIT:	LD	HL,0		; Return SUCCESS
	POP	IY
	RET		; Exit, exit code is in HL
;---------------------------------------------------------------
; Memory storage for START procedure
;
ARGP:	.BLKP	1	; ^ to CLI
WELCOM:	.ASCII	"Hello, programmer! 0.0.1 (c) 2023 Radek Hnilica\r\n"
	.ASCII	"$Id: break-ma.asm,v 1.2 2023-07-31 19:48:07+02 radek Exp $\r\n"
	.ASCIZ	"MOS command in ADL mode."

MESSAGE: .ASCIZ	"This is second call to VDU string.\r\n"

	.INCLUDE "/src/asmlib/show.regs.asm"
	.INCLUDE "/src/asmlib/p.hex24.asm"
	.INCLUDE "/src/asmlib/p.hex16.asm"
	.INCLUDE "/src/asmlib/p.hex8.asm"
	.INCLUDE "/src/asmlib/p.cstr.asm"
	.INCLUDE "/src/asmlib/p.nl.asm"

