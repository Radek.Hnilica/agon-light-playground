; $Id: hex2dec.asm,v 1.4 2023-08-04 17:38:09+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/misc/hex2dec.asm,v $
;
; Convert binary number to decimal and print
; Copyright (c) 2023 Radek Hnilica
;
; BUILD & INSTALL:
;	ez80asm hex2dec.asm /mos/hex2dec.bin -lsv
;
; RUN:
;	hex2dec <hex-number>
;	hex2dec ?
;

;---------------------------------------------------------------
; Macros for MOS and VDU call
;
; NOTE: Remember, even if we are in ADL mode, we must mangle
;	RST instruction with '.L'.

	; MOS api, the reg. A contain service number
	.MACRO	MOS fn
	LD	A,fn
	RST.L	$10
	.ENDMACRO

	; Sending one character, in register A, to VDP.
	.MACRO	VDU
	RST.L	$10
	.ENDMACRO

	.MACRO	VDU.C	char
	LD	A,char
	RST.L	$10
	.ENDMACRO

;---------------------------------------------------------------

	.FILLBYTE $00
	.ASSUME	ADL=1	; eZ80 ASL

	.ORG	$0B0000	; Where the MOS commands live in
	JR	START	; Where the real code lives

	; There is space from start of the program and MOS
	; header.  You can use it as you wish for code,
	; variables, messages, ...
	; Just don't forget not to run into MOS header!

;---------------------------------------------------------------
; MOS program header
	; there is space for code, data up to
	.ALIGN	$40	; Header is at offset $40
	.DB	"MOS",0,1	; MOS, version 0, 1-ADL mode

;---------------------------------------------------------------
; START - the real code of the application
; IN:	HL points at CLI
; OUT:	HL contains exit code
; NOTE:	Saves necessary registers
;
START:
	PUSH	IY		; Save IY or face consequences

	; On program/command start the HL contains address of
	; command line arguments.  So it's good idea to save it.
	LD	(ARGP),HL	; Save Argumeny Pointer to CLI
	LD	IX,(ARGP)

	CALL	SKIP_SP		; Skip possible spaces
	LD	A,(IX+0)
	CP	'?'
	JR	NZ,@F

	LD	HL,WELCOM
	CALL	P.CSTR
	CALL	P.NL
	JR	EXIT

@@:
	;LD	IX,(ARGP)
	CALL	XTOU24
	;LD	(ARGP),IX
	CALL	P.HEX24
	CALL	P.NL

	; Convert example number in HEX to DEC
	;LD	HL,$4C23FA
	LD	(BIN),HL
	CALL	U24TOBCD	; BIN,BCD	;ITOD
	LD	HL,BCD
	LD	B,8
	CALL	P.BCD		; HL=BCD,B=8
	CALL	P.NL

EXIT:	LD	HL,0		; Return SUCCESS
	POP	IY	; Restore IY register
	RET		; Exit, exit code is in HL
;---------------------------------------------------------------
; Memory storage for START procedure
;
ARGP:	.BLKP	1	; ^ to CLI
WELCOM:	.ASCII	"hex2dec (c) 2023 Radek Hnilica\r\n"
	.ASCIZ	"$Id: hex2dec.asm,v 1.4 2023-08-04 17:38:09+02 radek Exp $\r\n"

	.INCLUDE "/src/asmlib/p.nl.asm"
	.INCLUDE "/src/asmlib/p.cstr.asm"

;---------------------------------------------------------------
;
SKIP_SP:

	PUSH	BC
	LD	B,80		; Limit to CLI length
@LOOP:	LD	A,(IX+0)
	CP	' '
	JP	NZ,@EXIT

	INC	IX
	DJNZ	@LOOP

@EXIT:	POP	BC
	RET

;---------------------------------------------------------------
; DUMP - dump memory from address HL, B bytes
DUMP:
	CALL	P.HEX24
	.VDU.C	':'
@@:	.VDU.C	' '
	LD	A,(HL)
	CALL	P.HEX8
	INC	HL
	DJNZ	@B
	RET

;---------------------------------------------------------------
; DUMPREV - dump memory from address HL, B bytes, in reverse order
DUMPREV:
	PUSH	AF
	PUSH	BC
	PUSH	DE
	CALL	P.HEX24
	.VDU.C	':'
	;: HL+B-1
	LD	DE,0	; DE:=B
	LD	E,B	;+
	ADD	HL,DE	; HL:=HL+B
	DEC	HL	; HL:=HL+B-1

@@:	.VDU.C	' '
	LD	A,(HL)
	CALL	P.HEX8
	DEC	HL
	DJNZ	@B
	POP	DE
	POP	BC
	POP	AF
	RET


;---------------------------------------------------------------
; DBG - print information usefull for debuging the algorithm
;
DBG:
	PUSH	AF
	PUSH	BC
	PUSH	HL
	LD	A,B
	CALL	P.HEX8
	.VDU.C	'>'
	LD	B,8
	LD	HL,BCD
	CALL	DUMPREV
	.VDU.C	' '
	LD	B,3
	LD	HL,BIN
	CALL	DUMPREV
	CALL	P.NL
	POP	HL
	POP	BC
	POP	AF
	RET

;---------------------------------------------------------------
; Include subroutines from asmlib
;
	.INCLUDE "/src/asmlib/p.hex24.asm"
	.INCLUDE "/src/asmlib/p.hex16.asm"
	.INCLUDE "/src/asmlib/p.hex8.asm"
	.INCLUDE "/src/asmlib/xtou24.asm"
	.INCLUDE "/src/asmlib/toupper.asm"
	.INCLUDE "/src/asmlib/isxdigit.asm"
	.INCLUDE "/src/asmlib/u24tobcd.asm"
	.INCLUDE "/src/asmlib/p.bcd.asm"
