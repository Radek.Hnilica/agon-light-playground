;
; $Id: args.asm,v new 2023-07-21 $
; $Source$
; Print arguments given on command line
; Copyright (c) 2023 Radek Hnilica
;
; Build:
;   ez80asm args.asm args.bin -lsv
;
; Run:
;   load args.bin
;   run . some arguments

	.FILLBYTE $00
	.ASSUME	ADL=0	; for Z80 mode
	.ORG	$0000	; binary starts from address 0

; Z80 memory starts with code fro 8 restart vecotors.  These
; starts each 8th byte.  The space is thus only for few
; instructions and we put usually only 'JP' instruction there.

; Restart code on address 0 servers as starting of the program,
; usually by RESET signal.  However not on eZ80 CPU in ADL Z80
; mode.  For us it us the program starting address.
RST00:	JP	START		; jump to the START code

; Restart on address 8, caused by instruction 'RST 8' is used
; for calling MOS services.  These expect number of the service
; being in the A register and could also expect other arguments
; in register specific for that service call.
	.ALIGN	8
RST08:	RST.LIS	8	; trampoline to eZ80 mode MOS entry
	RET		; we still need to return

	.MACRO	MOS	SERVICE
	LD	A,SERVICE
	RST	8
	.ENDMACRO

; On address $10 is code for VDU call passing one character.
; We call VDU by instruction 'RST $10'.  The character for VDU
; is in register A.
	.ALIGN	8
RST10:	RST.LIS	$10	; trampoline to eZ80 mode VDU entry
	RET

	.MACRO	VDU	CHAR
	LD	A,CHAR
	RST	$10
	.ENDMACRO

; On address $18 is code for VDU passing the whole string.
	.ALIGN	8
RST18:	RST.LIS	$18
	RET

; On from address $20 are rest of the restart addresses.  They
; are not used yet (MOS version 1.03), but I put trampolines
; in their place for possible future us.  Actualy you can use
; these for your own purpose.
	.ALIGN	8
RST20:	RST.LIS $20
	RET

	.ALIGN	8
RST28:	RST.LIS	$28
	RET

	.ALIGN	8
RST30:	RST.LIS	$30
	RET

; The last restart is mentioned in documentation as dedicated
; fro NMI interrupt.  Up to my knowledge NMI in Z80 is on
; different address, but I let it be there as per documentation.
	.ALIGN	8
RST38:	EI
	RETI

; MOS program header.
	.ALIGN	$40		; the header starts at $40
	.DB	"MOS",0,0	; magic string, version, Z80

MY_HDR:	.ASCIZ	"args.prg"	


; Starting point of the program.  From MOS we get
;  eIX:	argv - pointer to array of parameters
;  C:	argc - number of parameters
START:
	EI		; ??? Why is this needed.
	PUSH.LIL HL	; Save the eHL
	LD	HL,S_HELLO
	CALL	PRSTR16
	LD	HL,S_eHL
	CALL	PRSTR16
	POP.LIL	HL
	PUSH.LIL HL
	CALL	PRHEX24
	VDU	'^'
	VDU	'='
	VDU	'"'
	POP.LIL	HL
	PUSH.LIL HL
	CALL	PRSTR24
	VDU	'"'
	CALL	PRCRLF

	POP.LIL	HL
	LD	B,32
@@:
	VDU	' '
	LD.L	A,(HL)
	CALL	PRHEX8
	INC.L	HL
	DJNZ	@B
	LD	HL,S_CRLF
	CALL	PRSTR16


	LD	HL,0	; Return from program with success.
	RET.L		; We must return using SPL!, OR ELSE!


; Print a zero terminated string pointed by 16-bit addr in HL
; Parameters:
;  HL:	Address of string
; Clobber:
;  A, HL
PRSTR16:
@@:	LD	A,(HL)
	OR	A
	RET	Z
	RST	$10	; VDU char
	INC	HL
	JR	@B

PRSTR24:
@@:	LD.LIL	A,(HL)
	OR	A
	RET	Z
	RST	$10
	INC.LIL	HL
	JR	@B

PRCRLF:
	VDU	'\r'
	VDU	'\n'
	RET


; Strings
S_HELLO:	.ASCIZ	"ARGS.BIN 0.1 2023-07-21\r\n"
S_eHL:		.ASCIZ	"eHL: "
S_CRLF:		.ASCIZ	"\n\r"


; Print 24-bit number in HEX
; Parameters:
;  eHL: the 24-bit number to print
; Destroys: A,F
PRHEX24:
	PUSH.LIL HL
	LD.LIL	HL,2
	ADD.LIL	HL,SP
	LD.LIL	A,(HL)
	POP.LIL	HL
	CALL	PRHEX8
	; pass through yo PRHEX16

; Print 16-bit number in HEX
; Parameters:
;  HL:	the 16-bit number to print
; Destroys:	A,F
PRHEX16:
	LD	A,H
	CALL	PRHEX8
	LD	A,L
	; pass through to PRHEX8

PRHEX8:
	PUSH	AF	; Save the original value in A
	RRA		; Rotate the high nible to D3..D0
	RRA		;+
	RRA		;+
	RRA		;+
	CALL	@F	; Print the high nible now in bits 3..0
	POP	AF	; Restore the A
	; pass throug to nible print
@@:	AND	$0F	; Mask the valit nible bits
	ADD	A,$90
	DAA
	ADC	A,$40
	DAA
	RST	$10	; VDU char
	RET

;        1111111111222222222233333333334444444444555555555566666
;234567890123456789012345678901234567890123456789012345678901234
