#!/bin/bash
# $Id$
# $Source$
# Pull source, listing, symbols, and binary from sd card.
# Copyright (c) 2023 Radek Hnilica

#set -xe

#declare -r SD=/media/radek/AEDE-11B1
declare -r SD=${HOME}/st/repo/agonl2/backup
declare -r PROJ=misc

cp -v $SD/src/${PROJ}/*.{asm,lst,symbols,bin} .
#cp -v $SD/src/sysvars/mos_api.inc .
