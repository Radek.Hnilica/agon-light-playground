PC     Output            Line Source
040000                   0001 ;
040000                   0002 ; $Id: hello-prg-z80.asm,v 1.1 2023-07-24 13:36:55+02 radek Exp radek $
040000                   0003 ; $Source: /home/radek/st/repo/agonl2/src/playground/hello/hello-prg-z80.asm,v $
040000                   0004 ; ==============================================================
040000                   0005 ; Hello program to be loaded into standard program area $40000.
040000                   0006 ; This program variant is to be run in Z80 mode.
040000                   0007 ;
040000                   0008 ; Copyright (c) 2023 Radek Hnilica
040000                   0009 ;
040000                   0010 ; BUILD:
040000                   0011 ;	ez80asm watchkbd.asm -l
040000                   0012 ;
040000                   0013 ; EXECUTE:
040000                   0014 ;	load watchkbd.bin
040000                   0015 ;	run
040000                   0016 ;
040000                   0017 
040000                   0018 ; -----   Localy define macros   -------------------------------
040000                   0019 ;
040000                   0020 ; .MOS -- invoke MOS, function number is expected to be in A
040000                   0021 	.MACRO	MOS
040000                   0022 	RST	8
040000                   0023 	.ENDMACRO
040000                   0024 
040000                   0025 ;---------------------------------------------------------------
040000                   0026 ; .MOSF fn -- invoke MOS function fn
040000                   0027 	.MACRO	MOSF	fn
040000                   0028 	LD	A,fn
040000                   0029 	RST	8
040000                   0030 	.ENDMACRO
040000                   0031 
040000                   0032 ;---------------------------------------------------------------
040000                   0033 ; .VDU -- send value in A to VDP
040000                   0034 	.MACRO	VDU
040000                   0035 	RST	$10
040000                   0036 	.ENDMACRO
040000                   0037 
040000                   0038 ;---------------------------------------------------------------
040000                   0039 ; .VDUC char -- send character of function code to VDP
040000                   0040 	.MACRO	VDUC	char
040000                   0041 	LD	A,char
040000                   0042 	RST	$10
040000                   0043 	.ENDMACRO
040000                   0044 
040000                   0045 	.MACRO	VDU.C	char
040000                   0046 	LD	A,char
040000                   0047 	RST	$10
040000                   0048 	.ENDMACRO
040000                   0049 
040000                   0050 ;---------------------------------------------------------------
040000                   0051 ; .VDUS .... -- send string of characters to VDP
040000                   0052 
040000                   0053 	.FILLBYTE 0
040000                   0054 	.ASSUME	ADL=0	; Z80 memory mode
040000                   0055 	.ORG	$0000	; From the start of Z80 memory
000000                   0056 
000000                   0057 ; =====   MOS API   ============================================
000000                   0058 mos_getkey:		.EQU	0
000000                   0059 mos_sysvars:		.EQU	$08
000000                   0060 
000000                   0061 sysvar_time:		.EQU	$00	; 3B in little endian
000000                   0062 sysvar_keyascii:	.EQU	$05	; ASCII or 0;  BROKEN
000000                   0063 sysvar_keymods:		.EQU	$06	; Kezcode modifiers
000000                   0064 sysvar_vkeycode:	.EQU	$17	; from FabGL
000000                   0065 sysvar_vkeydown:	.EQU	$18	; 0=up, 1=down
000000                   0066 sysvar_vkeycount:	.EQU	$19	; key packet counter
000000                   0067 
000000                   0068 ; =====   RST vectors   ========================================
000000                   0069 ; The Z80 memory started with eight RST vectors.  Here CPU lands
000000                   0070 ; when you execute 'RST n' instruction.  In actual version of
000000                   0071 ; Agon Quark MOS 1.03, only first four of them are used.
000000                   0072 
000000                   0073 ; -----   RST 0/RESET   ----------------------------------------
000000                   0074 ; Your program execution starts here.
000000                   0075 ;
000000 C3 45 00          0076 RST00:	JP	START	; Jump over the rest of RST vectors
000003                   0077 
000003                   0078 	; There are 5 unused bytes here.
000003                   0079 
000003                   0080 ;---------------------------------------------------------------
000003                   0081 ; Trampoline to RST.LIS into eZ80 program space
000003                   0082 
000003                   0083 
000003                   0084 ; ----- MOS api function call ----------------------------------
000003                   0085 ;
000003 00 00 00 00 00    0086 	.ALIGN	8
000008 49 CF             0087 RST08:	RST.L	$08	; Trampoline to MOS in ADL mode
00000A C9                0088 	RET
00000B                   0089 
00000B                   0090 ; ----- VDP character interface --------------------------------
00000B 00 00 00 00 00    0091 	.ALIGN	8
000010 49 D7             0092 RST10:	RST.L	$10	; VDU character
000012 C9                0093 	RET
000013                   0094 
000013                   0095 ; ----- VDP string interface -----------------------------------
000013 00 00 00 00 00    0096 	.ALIGN	8
000018 49 DF             0097 RST18:	RST.L	$18	; VDU string
00001A C9                0098 	RET
00001B                   0099 
00001B                   0100 ; You can define trampoline calls to eZ80 program space if you
00001B                   0101 ; want or use restarts RST20 RST28 RST30 RST38 whtever purposes
00001B                   0102 ; you want.  You can also use this memory for compeltely
00001B                   0103 ; different things.
00001B                   0104 
00001B                   0105 
00001B                   0106 ; =====   MOS header   =========================================
00001B                   0107 ;
00001B 00 00 00 00 00 00 0108 	.ALIGN	$40	; must start at offset $40 from load adr
       00 00 00 00 00 00 
       00 00 00 00 00 00 
       00 00 00 00 00 00 
       00 00 00 00 00 00 
       00 00 00 00 00 00 
       00                
000040 4D 4F 53 00 00    0109 MOSHDR:	.DB	"MOS",0,0	; MOS, ver 0, Z80 mode
000045                   0110 
000045                   0111 ; This is good place for configuration values because it starts
000045                   0112 ; at well defined address.
000045                   0113 
000045                   0114 
000045                   0115 ;----- Program Execution Starts Here ---------------------------
000045                   0116 
000045                   0117 START:
000045 ED 73 FF 00       0118 	LD	(SAVSP),SP	; Save SP so we can restore it.
000049                   0119 	;LD	SP,$FFFF	; Give SP meaningfull value
000049                   0120 
000049 21 A8 00          0121 	LD	HL,M.HELLO	; Wellcome message
00004C CD 9C 01          0122 	CALL	P.CSTR		;+
00004F                   0123 
00004F                   0124 	.MOSF	mos_sysvars	; Get the sysvar pointer to IXU
00004F 3E 08             0124 	LD	A,fn
000051 CF                0124 	RST	8
000052 5B DD 7E 19       0125 	LD.LIL	A,(IX+sysvar_vkeycount)	; Initialise the KYCNT
000056 3D                0126 	DEC	A			;+so it will trigger
000057 32 02 01          0127 	LD	(KYCNT),A		;+first SHOW.SYSVARS
00005A                   0128 
00005A 06 FF             0129 	LD	B,255		; Limit number of prints
00005C                   0130 LOOP:
00005C 5B DD 4E 19       0131 	LD.LIL	C,(IX+sysvar_vkeycount)	; Was there any new
000060 3A 02 01          0132 	LD	A,(KYCNT)		;+key packet received?
000063 B9                0133 	CP	C			;+
000064 28 0B             0134 	JR	Z,@NOPRT		; NO: skip the print
000066 79                0135 	LD	A,C			; YES: update counter
000067 32 02 01          0136 	LD	(KYCNT),A		;+
00006A CD 39 01          0137 	CALL	SHOW.SYSVARS		; And print
00006D CD A7 01          0138 	CALL	P.NL
000070 05                0139 	DEC	B		; Decrement loop print counter
000071                   0140 @NOPRT:
000071 CD 80 01          0141 	CALL	DELAY
000074 CD 80 01          0142 	CALL	DELAY
000077 78                0143 	LD	A,B
000078 B0                0144 	OR	B
000079 20 E1             0145 	JR	NZ,LOOP
00007B C3 9F 00          0146 	JP	EXIT
00007E                   0147 
00007E                   0148 
00007E                   0149 
00007E CD 03 01          0150 	CALL	KEYINI
000081                   0151 
000081                   0152 @LOOP:
000081 CD 16 01          0153 	CALL	GETKEY
000084 B7                0154 	OR	A
000085 20 10             0155 	JR	NZ,@HASKEY
000087                   0156 @NOKEY:
000087 21 01 01          0157 	LD	HL,KEY
00008A BE                0158 	CP	(HL)
00008B 28 08             0159 	JR	Z,@SAME
00008D                   0160 	;not same
00008D                   0161 	.VDUC	'.'
00008D 3E 2E             0161 	LD	A,char
00008F D7                0161 	RST	$10
000090 3E 00             0162 	LD	A,0
000092 32 01 01          0163 	LD	(KEY),A
000095                   0164 
000095 18 EA             0165 @SAME:	JR	@LOOP
000097                   0166 
000097                   0167 
000097                   0168 
000097                   0169 @HASKEY:
000097                   0170 	; Key in A
000097 32 01 01          0171 	LD	(KEY),A
00009A                   0172 	.VDU
00009A D7                0172 	RST	$10
00009B FE 71             0173 	CP	'q'		; Terminate program on 'q'
00009D 20 E2             0174 	JR	NZ,@LOOP	; If no 'q' loop
00009F                   0175 
00009F                   0176 	; Return to MOS with success
00009F 21 00 00          0177 EXIT:	LD	HL,0
0000A2 ED 7B FF 00       0178 	LD	SP,(SAVSP)
0000A6 49 C9             0179 	RET.L
0000A8                   0180 
0000A8                   0181 
0000A8                   0182 ;---------------------------------------------------------------
0000A8                   0183 ; DATA AREA
0000A8 48 65 6C 6C 6F 2C 0184 M.HELLO:	.ASCII	"Hello, Friend!\r\n"
       20 46 72 69 65 6E 
       64 21 0D 0A       
0000B8 24 49 64 3A 20 77 0185 		.ASCII  "$Id: watchkbd.asm,v new.0 2023-08-25 $\r\n"
       61 74 63 68 6B 62 
       64 2E 61 73 6D 2C 
       76 20 6E 65 77 2E 
       30 20 32 30 32 33 
       2D 30 38 2D 32 35 
       20 24 0D 0A       
0000E0 53 74 75 64 79 20 0186 		.ASCIZ	"Study behaviour of keyboard.\r\n"
       62 65 68 61 76 69 
       6F 75 72 20 6F 66 
       20 6B 65 79 62 6F 
       61 72 64 2E 0D 0A 
       00                
0000FF                   0187 
0000FF 00 00             0188 SAVSP:	.BLKW	1		; Keep original SP for returning
000101 00                0189 KEY:	.BLKB	1
000102 00                0190 KYCNT:	.DB	0
000103                   0191 
000103                   0192 
000103                   0193 ;---------------------------------------------------------------
000103                   0194 ; Initialise keyboard code
000103                   0195 ;
000103                   0196 KEYINI:
000103                   0197 	.MOSF	mos_sysvars	; Get the sysvar pointer to IXU
000103 3E 08             0197 	LD	A,fn
000105 CF                0197 	RST	8
000106 5B DD 7E 18       0198 @@:	LD.LIL	A,(IX+sysvar_vkeydown)	; Wait for Key Release
00010A B7                0199 	OR	A			;+
00010B 20 F9             0200 	JR	NZ,@B			;+
00010D 5B DD 7E 19       0201 	LD.LIL	A,(IX+sysvar_vkeycount)	; Update packet counter
000111 32 15 01          0202 	LD	(KEYCNT),A		;+
000114 C9                0203 	RET
000115                   0204 
000115 00                0205 KEYCNT:	.BLKB	1		; counter
000116                   0206 
000116                   0207 ;---------------------------------------------------------------
000116                   0208 ; GETKEY -- read keyboard as ASCII into A, return 0 if no input
000116                   0209 ;
000116                   0210 GETKEY:
000116                   0211 	.MOSF	mos_sysvars	; Get the sysvar pointer to IXU
000116 3E 08             0211 	LD	A,fn
000118 CF                0211 	RST	8
000119 3A 15 01          0212 	LD	A,(KEYCNT)	; Did packet counter chage
00011C 5B DD BE 19       0213 	CP.LIL	(IX+sysvar_vkeycount)
000120 28 14             0214 	JR	Z,@NOKEY
000122                   0215 
000122                   0216 	.MOSF	mos_getkey	; Read the new key
000122 3E 00             0216 	LD	A,fn
000124 CF                0216 	RST	8
000125 F5                0217 	PUSH	AF		; and save for return
000126                   0218 
000126 5B DD 7E 18       0219 @@:	LD.LIL	A,(IX+sysvar_vkeydown)	; Wait for Key Release
00012A B7                0220 	OR	A			;+
00012B 20 F9             0221 	JR	NZ,@B			;+
00012D 5B DD 7E 19       0222 	LD.LIL	A,(IX+sysvar_vkeycount)	; update packet counter
000131 32 15 01          0223 	LD	(KEYCNT),A		;+
000134 F1                0224 	POP	AF		; Return the read key
000135 C9                0225 	RET			;+
000136                   0226 
000136 3E 00             0227 @NOKEY:	LD	A,0		; No key was given
000138 C9                0228 	RET			;+
000139                   0229 
000139                   0230 
000139                   0231 
000139                   0232 ;---------------------------------------------------------------
000139                   0233 ; SHOW.SYSVARS - Show system variables mainly keyboard related
000139                   0234 ; IN:	IXU - points to sysvars, get by MOS mos_sysvars
000139                   0235 ; OUT:	display
000139                   0236 ;
000139                   0237 SHOW.SYSVARS:
000139 F5                0238 	PUSH	AF		; Save registers
00013A                   0239 	.VDUC	'T'
00013A 3E 54             0239 	LD	A,char
00013C D7                0239 	RST	$10
00013D 5B DD 7E 02       0240 	LD.LIL	A,(IX+sysvar_time+2)
000141 CD B3 01          0241 	CALL	P.X8
000144 5B DD 7E 01       0242 	LD.LIL	A,(IX+sysvar_time+1)
000148 CD B3 01          0243 	CALL	P.X8
00014B 5B DD 7E 00       0244 	LD.LIL	A,(IX+sysvar_time+0)
00014F CD B3 01          0245 	CALL	P.X8
000152                   0246 
000152                   0247 	.VDUC	' '
000152 3E 20             0247 	LD	A,char
000154 D7                0247 	RST	$10
000155                   0248 	.VDUC	'K'
000155 3E 4B             0248 	LD	A,char
000157 D7                0248 	RST	$10
000158 5B DD 7E 05       0249 	LD.LIL	A,(IX+sysvar_keyascii)
00015C CD B3 01          0250 	CALL	P.X8
00015F 5B DD 7E 06       0251 	LD.LIL	A,(IX+sysvar_keymods)
000163 CD B3 01          0252 	CALL	P.X8
000166                   0253 
000166                   0254 	.VDUC	' '
000166 3E 20             0254 	LD	A,char
000168 D7                0254 	RST	$10
000169 5B DD 7E 17       0255 	LD.LIL	A,(IX+sysvar_vkeycode)
00016D CD B3 01          0256 	CALL	P.X8
000170 5B DD 7E 18       0257 	LD.LIL	A,(IX+sysvar_vkeydown)
000174 CD B3 01          0258 	CALL	P.X8
000177 5B DD 7E 19       0259 	LD.LIL	A,(IX+sysvar_vkeycount)
00017B CD B3 01          0260 	CALL	P.X8
00017E F1                0261 	POP	AF		; Restore registers
00017F C9                0262 	RET
000180                   0263 
000180                   0264 
000180                   0265 ; ----- DELAY - Wait for some time -----------------------------
000180                   0266 ;
000180                   0267 DELAY:
000180 F5                0268 	PUSH	AF		; Save used registers
000181 C5                0269 	PUSH	BC		;+
000182 01 FF FF          0270 	LD	BC,$FFFF
000185 00                0271 @@:	NOP
000186 0B                0272 	DEC	BC
000187 78                0273 	LD	A,B
000188 B1                0274 	OR	C
000189 20 FA             0275 	JR	NZ,@B
00018B C1                0276 	POP	BC		; Restore used registers
00018C F1                0277 	POP	AF
00018D C9                0278 	RET
00018E                   0279 
00018E                   0280 LDELAY:
00018E C5                0281 	PUSH	BC
00018F 01 10 00          0282 	LD	BC,$10
000192 CD 80 01          0283 @@:	CALL	DELAY
000195 0B                0284 	DEC	BC
000196 78                0285 	LD	A,B
000197 B1                0286 	OR	C
000198 20 F8             0287 	JR	NZ,@B
00019A C1                0288 	POP	BC
00019B C9                0289 	RET
00019C                   0290 
00019C                   0291 ; =====   Include subroutines from ASMLIB   ====================
00019C                   0292 ;
00019C                   0293 	.INCLUDE "/lib/asm/p.cstr.inc"
00019C                   0001 ; $Id: p.cstr.inc,v 1.2 2023-08-07 12:08:55+02 radek Exp $
00019C                   0002 ; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.cstr.inc,v $
00019C                   0003 ;---------------------------------------------------------------
00019C                   0004 ; P.CSTR - print zero terminated (C-Lang) string at address HL
00019C                   0005 ; IN:	HL - string address
00019C                   0006 ; OUT:	screen
00019C                   0007 ; DESTROY: -
00019C                   0008 ;
00019C                   0009 P.CSTR:
00019C F5                0010 	PUSH	AF		; Save used registers
00019D C5                0011 	PUSH	BC		;+
00019E 01 00 00          0012 	LD	BC,0		; use delimiter
0001A1 AF                0013 	XOR	A		; 0 as delimiter
0001A2 49 DF             0014 	RST.L	$18		; VDU string
0001A4 C1                0015 	POP	BC		; Restore used registers
0001A5 F1                0016 	POP	AF		;+
0001A6 C9                0017 	RET
0001A7                   0294 	.INCLUDE "/lib/asm/p.nl.inc"
0001A7                   0001 ; $Id: p.nl.inc,v 1.4 2023-08-07 21:12:41+02 radek Exp $
0001A7                   0002 ; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.nl.inc,v $
0001A7                   0003 ;---------------------------------------------------------------
0001A7                   0004 ; P.NL - Print New Line (CRLF)
0001A7                   0005 ; IN:	-
0001A7                   0006 ; OUT:	screen
0001A7                   0007 ; DESTROY: AF
0001A7                   0008 ; DEPENDS: -
0001A7                   0009 ; NOTE:	Z80 safe
0001A7                   0010 ;
0001A7                   0011 P.NL:
0001A7                   0012 ;@ENTR: PUSH	AF		; Save used registers
0001A7                   0013 	.VDU.C	'\r'
0001A7 3E 0D             0013 	LD	A,char
0001A9 D7                0013 	RST	$10
0001AA                   0014 	.VDU.C	'\n'
0001AA 3E 0A             0014 	LD	A,char
0001AC D7                0014 	RST	$10
0001AD                   0015 ;@LEAV: POP	AF		; Restore used registers
0001AD C9                0016 	RET
0001AE                   0295 	.INCLUDE "/lib/asm/p.x16.inc"
0001AE                   0001 ; $Id: p.x16.inc,v 1.3 2023-08-07 21:34:26+02 radek Exp $
0001AE                   0002 ; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.x16.inc,v $
0001AE                   0003 ;---------------------------------------------------------------
0001AE                   0004 ; P.X16 - print 16-bit number in HL in hexadecimal
0001AE                   0005 ; IN:	HL - 16 bit word to print
0001AE                   0006 ; OUT:	screen
0001AE                   0007 ; DESTROY: AF
0001AE                   0008 ; DEPENDS: P.X8, P.X4
0001AE                   0009 ; NOTE: Z80 safe
0001AE                   0010 ;
0001AE                   0011 P.X16:
0001AE 7C                0012 	LD	A,H
0001AF CD B3 01          0013 	CALL	P.X8
0001B2 7D                0014 	LD	A,L
0001B3                   0015 	;pass to P.X8
0001B3                   0016 	;.INCLUDE "/lib/asm/P.X8.ASM"
0001B3                   0296 	.INCLUDE "/lib/asm/p.x8.inc"
0001B3                   0001 ; $Id: p.x8.inc,v 1.3 2023-08-07 21:29:02+02 radek Exp $
0001B3                   0002 ; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.x8.inc,v $
0001B3                   0003 ;---------------------------------------------------------------
0001B3                   0004 ; P.X8 - Print byte in A as two digit hexadecimal number
0001B3                   0005 ; IN:	A - byte to print
0001B3                   0006 ; OUT:	screen
0001B3                   0007 ; DESTROY: A,F
0001B3                   0008 ; DEPENDS: P.X4
0001B3                   0009 ;
0001B3                   0010 P.X8:
0001B3 F5                0011 	PUSH	AF		; Remember A
0001B4 1F                0012 	RRA			; Put high nible to low nible
0001B5 1F                0013 	RRA			;+
0001B6 1F                0014 	RRA			;+
0001B7 1F                0015 	RRA			;+
0001B8 CD BC 01          0016 	CALL	P.X4		; Print the high nible
0001BB F1                0017 	POP	AF		; Recover A, we need low nible
0001BC                   0018 	;pass to P.X4		; print low nible
0001BC                   0019 
0001BC                   0020 	;IMPORT /lib/asm/P.X4.INC
0001BC                   0021 
0001BC                   0297 	.INCLUDE "/lib/asm/p.x4.inc"
0001BC                   0001 ; $Id: p.x4.inc,v 1.3 2023-08-07 21:25:05+02 radek Exp $
0001BC                   0002 ; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/src/p.x4.inc,v $
0001BC                   0003 ;---------------------------------------------------------------
0001BC                   0004 ; P.X4 -- Print low nibble in A as hexadecimal digit
0001BC                   0005 ; IN:	A - nibble to print
0001BC                   0006 ; OUT:	screen
0001BC                   0007 ; DESTROY: A,F
0001BC                   0008 ; DEPENDS: -
0001BC                   0009 ;
0001BC                   0010 P.X4:
0001BC E6 0F             0011 	AND	$0F		; Mask low bits
0001BE C6 90             0012 	ADD	A,$90
0001C0 27                0013 	DAA
0001C1 CE 40             0014 	ADC	A,$40
0001C3 27                0015 	DAA
0001C4                   0016 	.VDU			; print ASCII xdigit
0001C4 D7                0016 	RST	$10
0001C5 C9                0017 	RET
0001C6                   0018 
0001C6                   0298 	.INCLUDE "/lib/asm/p.mem.inc"
0001C6                   0001 ; $Id: p.mem.asm,v 1.1 2023-08-04 15:34:06+02 radek Exp $
0001C6                   0002 ; $Source: /home/radek/st/repo/agonl2/src/playground/asmlib/p.mem.asm,v $
0001C6                   0003 ;---------------------------------------------------------------
0001C6                   0004 ; P.MEM - print content of memory given by HL in hexadecimal
0001C6                   0005 ;       memory is printed as sequence of non separated bytes
0001C6                   0006 ; IN:	HL - address of memory
0001C6                   0007 ;       B - count / number of bytes to print
0001C6                   0008 ; OUT:	screen, HL
0001C6                   0009 ; DESTROY: AF, B
0001C6                   0010 ; DEPENDS: P.X8, P.X4
0001C6                   0011 ;
0001C6                   0012 P.MEM:
0001C6 F5                0013 @ENTR:	PUSH	AF
0001C7 7E                0014 @LOOP:	LD	A,(HL)		; get byte
0001C8 CD B3 01          0015 	CALL	P.X8		; print it
0001CB 23                0016 	INC	HL		; advance HL to next byte
0001CC 10 F9             0017 	DJNZ	@LOOP		;
0001CE F1                0018 @LEAV:	POP	AF
0001CF C9                0019 	RET
0001D0                   0020 	;INCLUDE "/lib/asm/p.x8.inc
0001D0                   0299 
