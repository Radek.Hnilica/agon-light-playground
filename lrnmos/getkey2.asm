; $Id: getkey2.asm,v new.1 2023-07-24 13:36:55+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/hello/hello-prg-z80.asm,v $
; ==============================================================
; Read the keyboard.
;
; Copyright (c) 2023 Radek Hnilica
;
; BUILD:
;	ez80asm getkey2.asm -l
;
; EXECUTE:
;	load getkey2.bin
;	run
;

; =====   MOS API   ============================================
	.INCLUDE "/lib/asm/mosapi.inc"

;---------------------------------------------------------------
; .MOSF fn -- invoke MOS function fn
	.MACRO	MOSF	fn
	LD	A,fn
	RST	8
	.ENDMACRO


	.FILLBYTE 0
	.ASSUME	ADL=0	; Z80 memory mode
	.ORG	$0000	; From the start of Z80 memory



; =====   RST vectors   ========================================
; The Z80 memory started with eight RST vectors.  Here CPU lands
; when you execute 'RST n' instruction.  In actual version of
; Agon Quark MOS 1.03, only first four of them are used.

; -----   RST 0/RESET   ----------------------------------------
; Your program execution starts here.
;
RST00:	JP	START	; Jump over the rest of RST vectors

	; There are 5 unused bytes here.

;---------------------------------------------------------------
; Trampoline to RST.LIS into eZ80 program space


; ----- MOS api function call ----------------------------------
;
	.ALIGN	8
RST08:	RST.L	$08	; Trampoline to MOS in ADL mode
	RET

; ----- VDP character interface --------------------------------
	.ALIGN	8
RST10:	RST.L	$10	; VDU character
	RET

; ----- VDP string interface -----------------------------------
	.ALIGN	8
RST18:	RST.L	$18	; VDU string
	RET

; You can define trampoline calls to eZ80 program space if you
; want or use restarts RST20 RST28 RST30 RST38 whtever purposes
; you want.  You can also use this memory for compeltely
; different things.


; =====   MOS header   =========================================
;
	.ALIGN	$40	; must start at offset $40 from load adr
MOSHDR:	.DB	"MOS",0,0	; MOS, ver 0, Z80 mode

; This is good place for configuration values because it starts
; at well defined address.


;----- Program Execution Starts Here ---------------------------

START:
	LD	(SAVSP),SP
	LD	SP,$FFFE	; Give SP meaningfull value

	LD	HL,M.HELLO
	CALL	P.CSTR

@LOOP:
	.MOSF	getkey
	CP	$0D	; RETURN
	JR	NZ,@F
	CALL	P.NL
@@:
	CP	'Q'
	JR	Z,EXIT

	PUSH	AF
	CALL	P.X8
	.VDUC	' '
	POP	AF
	.VDU

	JR	@LOOP


	; Return to MOS with success
EXIT:	LD	HL,0
	LD	SP,(SAVSP)
	RET.L


;---------------------------------------------------------------
; DATA AREA
M.HELLO:	.ASCII	"Hello, Friend!\r\n"
		.ASCII  "$Id: getkey2.asm,v new $\r\n"
		.ASCIZ	"Demonstration of reading keyboard.\r\n"

SAVSP:	.BLKW	1		; Keep original SP for returning

; =====   Include subroutines from ASMLIB   ====================
;
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "/lib/asm/p.nl.inc"
	;.INCLUDE "/lib/asm/p.x16.inc"
	.INCLUDE "/lib/asm/p.x8.inc"
	.INCLUDE "/lib/asm/p.x4.inc"
	;.INCLUDE "/lib/asm/p.mem.inc"

