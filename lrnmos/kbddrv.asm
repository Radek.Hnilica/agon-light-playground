; $Id: kbddrv.asm,v 1.1 2023-07-24 13:36:55+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/hello/hello-prg-z80.asm,v $
; ==============================================================
; Implementation and demostration of mykbd keyboard driver.
;
; Copyright (c) 2023 Radek Hnilica
;
; BUILD:
;	ez80asm kbddrv.asm -l
;
; EXECUTE:
;	load kbddrv.bin
;	run
;

; -----   Localy define macros   -------------------------------
;
; .MOS -- invoke MOS, function number is expected to be in A
	.MACRO	MOS
	RST	8
	.ENDMACRO

;---------------------------------------------------------------
; .MOSF fn -- invoke MOS function fn
	.MACRO	MOSF	fn
	LD	A,fn
	RST	8
	.ENDMACRO

;---------------------------------------------------------------
; .VDU -- send value in A to VDP
	.MACRO	VDU
	RST	$10
	.ENDMACRO

;---------------------------------------------------------------
; .VDUC char -- send character of function code to VDP
	.MACRO	VDUC	char
	LD	A,char
	RST	$10
	.ENDMACRO

	.MACRO	VDU.C	char
	LD	A,char
	RST	$10
	.ENDMACRO

;---------------------------------------------------------------
; .VDUS .... -- send string of characters to VDP 

	.FILLBYTE 0
	.ASSUME	ADL=0	; Z80 memory mode
	.ORG	$0000	; From the start of Z80 memory

; =====   MOS API   ============================================
mos_getkey:		.EQU	0
mos_sysvars:		.EQU	$08

sysvar_time:		.EQU	$00	; 3B in little endian
sysvar_keyascii:	.EQU	$05	; ASCII or 0;  BROKEN
sysvar_keymods:		.EQU	$06	; Kezcode modifiers
sysvar_vkeycode:	.EQU	$17	; from FabGL
sysvar_vkeydown:	.EQU	$18	; 0=up, 1=down
sysvar_vkeycount:	.EQU	$19	; key packet counter

; =====   RST vectors   ========================================
; The Z80 memory started with eight RST vectors.  Here CPU lands
; when you execute 'RST n' instruction.  In actual version of
; Agon Quark MOS 1.03, only first four of them are used.

; -----   RST 0/RESET   ----------------------------------------
; Your program execution starts here.
;
RST00:	JP	START	; Jump over the rest of RST vectors

	; There are 5 unused bytes here.

;---------------------------------------------------------------
; Trampoline to RST.LIS into eZ80 program space


; ----- MOS api function call ----------------------------------
;
	.ALIGN	8
RST08:	RST.L	$08	; Trampoline to MOS in ADL mode
	RET

; ----- VDP character interface --------------------------------
	.ALIGN	8
RST10:	RST.L	$10	; VDU character
	RET

; ----- VDP string interface -----------------------------------
	.ALIGN	8
RST18:	RST.L	$18	; VDU string
	RET

; You can define trampoline calls to eZ80 program space if you
; want or use restarts RST20 RST28 RST30 RST38 whtever purposes
; you want.  You can also use this memory for compeltely
; different things.


; =====   MOS header   =========================================
;
	.ALIGN	$40	; must start at offset $40 from load adr
MOSHDR:	.DB	"MOS",0,0	; MOS, ver 0, Z80 mode

; This is good place for configuration values because it starts
; at well defined address.


;----- Program Execution Starts Here ---------------------------

START:
	LD	(SAVSP),SP	; Save SP so we can restore it.
	;LD	SP,$FFFF	; Give SP meaningfull value

	LD	HL,M.HELLO	; Wellcome message
	CALL	P.CSTR		;+

	.MOSF	mos_sysvars	; Get the sysvar pointer to IXU

@LOOP:
	CALL	GETKEY
	OR	A
	JR	Z,@LOOP

	.VDU

	CP	$0D
	JR	NZ,@F
	.VDUC	$0A
	JR	@LOOP
	
@@:	CP	'q'		; Terminate program on 'q'
	JR	NZ,@LOOP	; If no 'q' loop

	CALL	P.NL

	; Return to MOS with success
EXIT:	LD	HL,0
	LD	SP,(SAVSP)
	RET.L


;---------------------------------------------------------------
; DATA AREA
M.HELLO:	.ASCII	"Keyboard driver and demo.\r\n"
		.ASCII  "$Id: mykbd.asm,v new.0 2023-08-25 $\r\n"
		.ASCII	"Copyright (c) 2023 Radek Hnilica.\r\n"
		.ASCIZ	"Type any text, 'q' terminate the program\r\n"

SAVSP:	.BLKW	1		; Keep original SP for returning
KEY:	.BLKB	1
KYCNT:	.DB	0
KEVCNT:	.DB	0		; Keboard event counter


;---------------------------------------------------------------
; Initialise keyboard code
;
KEYINI:
	.MOSF	mos_sysvars	; Get the sysvar pointer to IXU
@@:	LD.LIL	A,(IX+sysvar_vkeydown)	; Wait for Key Release
	OR	A			;+
	JR	NZ,@B			;+
	LD.LIL	A,(IX+sysvar_vkeycount)	; Update packet counter
	LD	(KEYCNT),A		;+
	RET

KEYCNT:	.BLKB	1		; counter

;---------------------------------------------------------------
; GETKEY -- read keyboard as ASCII into A, return 0 if no input
;
GETKEY:
	PUSH	BC
	DI	; Should I disable interrupt?

@IF:
	LD.LIL	A,(IX+sysvar_vkeycount)
	LD	B,A

	LD	A,(KEVCNT)
	CP	B
	JR	Z,@ELSE

	LD.LIL	A,(IX+sysvar_vkeydown)
	OR	A
	JR	NZ,@ELSE
@THEN:
	LD	A,B
	LD	(KEVCNT),A
	LD.LIL	A,(IX+sysvar_keyascii)
	JR	@FI
@ELSE:
	LD	A,0
@FI:
	EI	; Enable interrupt if disabled
	POP	BC
	RET			;+

;---------------------------------------------------------------
; SHOW.SYSVARS - Show system variables mainly keyboard related
; IN:	IXU - points to sysvars, get by MOS mos_sysvars
; OUT:	display
;
SHOW.SYSVARS:
	PUSH	AF		; Save registers
	.VDUC	'T'
	LD.LIL	A,(IX+sysvar_time+2)
	CALL	P.X8
	LD.LIL	A,(IX+sysvar_time+1)
	CALL	P.X8
	LD.LIL	A,(IX+sysvar_time+0)
	CALL	P.X8

	.VDUC	' '
	.VDUC	'K'
	LD.LIL	A,(IX+sysvar_keyascii)
	CALL	P.X8
	LD.LIL	A,(IX+sysvar_keymods)
	CALL	P.X8

	.VDUC	' '
	LD.LIL	A,(IX+sysvar_vkeycode)
	CALL	P.X8
	LD.LIL	A,(IX+sysvar_vkeydown)
	CALL	P.X8
	LD.LIL	A,(IX+sysvar_vkeycount)
	CALL	P.X8
	POP	AF		; Restore registers
	RET


; ----- DELAY - Wait for some time -----------------------------
;
DELAY:
	PUSH	AF		; Save used registers
	PUSH	BC		;+
	LD	BC,$FFFF
@@:	NOP
	DEC	BC
	LD	A,B
	OR	C
	JR	NZ,@B
	POP	BC		; Restore used registers
	POP	AF
	RET

LDELAY:
	PUSH	BC
	LD	BC,$10
@@:	CALL	DELAY
	DEC	BC
	LD	A,B
	OR	C
	JR	NZ,@B
	POP	BC
	RET

; =====   Include subroutines from ASMLIB   ====================
;
	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "/lib/asm/p.nl.inc"
	.INCLUDE "/lib/asm/p.x16.inc"
	.INCLUDE "/lib/asm/p.x8.inc"
	.INCLUDE "/lib/asm/p.x4.inc"
	.INCLUDE "/lib/asm/p.mem.inc"

