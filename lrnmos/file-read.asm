; $Id: file-read.asm,v 1.1 2023-08-29 18:00:00 radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/hello/hello-mos-adl.asm,v $
;
; Read file using MOS calls.
; Copyright (c) 2023 Radek Hnilica
;
; BUILD & INSTALL:
;	ez80asm file-read.asm /mos/test-file-read.bin -lsv
;
; RUN:
;	test-file-read
;
;---------------------------------------------------------------
; Macros for MOS and VDU call
	.INCLUDE "/lib/asm/mosapi.inc"

	.MACRO	MOSF	fn
	LD	A,fn
	RST.L	8
	.ENDMACRO

mos_fopen:	.EQU	$0A	; Get a file handle
mos_fclose:	.EQU	$0B	; Close a file handle
mos_fgetc:	.EQU	$0C	; Get a character from an open file
mos_fputc:	.EQU	$0D	; Write a character to an open file
mos_feof:	.EQU	$0E	; Check for end of file
mos_fread:	.EQU	$1A	; Read a block of data from file
mos_fwrite:	.EQU	$1B	; Write a block of data to a file
mos_flseek:	.EQU	$1C	; Move the read/write pointer in file

fa_read:		.EQU	1
fa_write:		.EQU	2
fa_open_existing:	.EQU	0
fa_create_new:		.EQU	4
fa_create_always:	.EQU	8
fa_open_always:		.EQU	$10
fa_open_append:		.EQU	$30
;---------------------------------------------------------------

	.FILLBYTE $00
	.ASSUME	ADL=1	; eZ80 ASL

	.ORG	$0B0000	; Where the MOS commands live in
	JR	START	; Where the real code lives

	; There is space from start of the program and MOS
	; header.  You can use it as you wish for code,
	; variables, messages, ...
	; Just don't forget not to run into MOS header!


;
;        1111111111222222222233333333334444444444555555555566666
;234567890123456789012345678901234567890123456789012345678901234
;

;---------------------------------------------------------------
; MOS program header
	; there is space for code, data up to
	.ALIGN	$40		; Header is at offset $40
	.DB	"MOS",0,1	; MOS, version 0, 1-ADL mode

;---------------------------------------------------------------
; START - the real code of the application
; IN:	HL points at CLI
; OUT:	HL contains exit code
; NOTE:	Saves necessary registers
;
START:
	PUSH	AF		; Be paranoid and save used
	PUSH	BC		;+registers.
	PUSH	DE		;+
	PUSH	IX		;+
	PUSH	IY		;+
	;EI	; Why?

	; On program/command start the HL contains address of
	; command line arguments.  So it's good idea to save it.
	LD	(ARGP),HL	; Save Argumeny Pointer to CLI
	
	LD	HL,WELCOM
	CALL	P.CSTR
	CALL	P.NL

	;Open File
	LD	C,fa_read
	LD	HL,file1_name
	.MOSF	mos_fopen
	
	OR	A
	JR	NZ,@F
	LD	HL,Cant_open
	CALL	P.CSTR
	CALL	P.NL
	JP	EXIT
file1_name:	.ASCIZ	"file-read.asm"
Cant_open:	.ASCIZ	"Can't open input file!"

@@:	
	LD	(FH1),A		; Save the file handler
	;LD	C,A
	CALL	P.X8		; Print the file handle
	CALL	P.NL


	; Print the file on the screen character by character
LOOP:
	; Check for EOF
	LD	A,(FH1)
	LD	C,A
	.MOSF	mos_feof
	OR	A
	JR	NZ,ENDLOOP


	; Read Character
	LD	A,(FH1)
	LD	C,A
	.MOSF	mos_fgetc

	;*FIXME: Handle TABs by expanding them.

	.VDU		; Print the character in A

	JR	LOOP
ENDLOOP:




	; Close file
	LD	A,(FH1)
	LD	C,A
	.MOSF	mos_fclose

	CALL	P.X8
	CALL	P.NL

	LD	HL,@DONE
	CALL	P.CSTR
	JR	@F

@DONE:	.ASCIZ	"DONE\r\n"
@@:

EXIT:	LD	HL,0	; Return SUCCESS
	POP	IY	; Restore registers because we saved
	POP	IX	;+them.
	POP	DE	;+
	POP	BC	;+
	POP	AF	;+
	RET		; Exit, exit code is in HL
;---------------------------------------------------------------
; Memory storage for START procedure
;
ARGP:	.BLKP	1	; ^ to CLI
FH1:	.BLKB	1	; File Handle

;
;        1111111111222222222233333333334444444444555555555566666
;234567890123456789012345678901234567890123456789012345678901234
;
WELCOM:	.ASCII	"Hello, programmer! 0.0.1 (c) 2023 Radek Hnilica\r\n"
	.ASCII	"$Id: file-read.asm,v new.1 2023-08-28 18:00:41+02 radek Exp $\r\n"
	.ASCIZ	"Reading file."


; ==============================================================
; Includes from asm library

	.INCLUDE "/lib/asm/p.cstr.inc"
	.INCLUDE "/lib/asm/p.nl.inc"

	.INCLUDE "/lib/asm/p.x8.inc"
	.INCLUDE "/lib/asm/p.x4.inc"
