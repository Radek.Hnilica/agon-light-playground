; $Id: hello-mos-z80.asm,v 1.2 2023-07-24 13:34:55+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/hello/hello-mos-z80.asm,v $
; Hello World in ASM in Z80 mode as MOS command
; Copyright (c) 2023 Radek Hnilica
;
; BUILD:
;	ez80asm hello-mos-z80.asm hello-mos-z80.bin -lsv
;
; INSTALL:
;	delete /mos/hello-mos-z80.bin
;	copy hello-mos-z80.bin /mos/hello-mos-z80.bin
;
; RUN:
;	hello-mos-z80
;

		.fillbyte $00
		.assume	adl=0		; For legacy Z80 mode
		.org	$0000		; code address for MOS command
		jp	start

		; RST vectors

		; MOS call
		.align	8
rst_08:		rst.lis	8
		ret

		; VDU char
		.align	8
rst_10:		rst.lis	$10
		ret

		; VDU string
		.align	8
rst_18:		rst.lis $18
		ret

		.align	8
rst_20:		rst.lis	$20
		ret

		.align	8
rst_28:		rst.lis	$28
		ret

		.align	8
rst_30:		rst.lis	$30
		ret

		; NMI
		.align	8
		ei
		reti


; MOS program header
		; there is space for code, data up to
		.align	$40
		.db	"MOS"		; MOS command magic number
		.db	0		; header version
		.db	0		; Z80 binary

start:
		ld	a,mb
		cp	$0B
		jr	nz,start1

		ld	ix,0
		add	ix,sp
		push.lis ix
		ld	sp,$7FFE


start1:		push.l	af		; push Mbase register onto SPL stack
		ei
		call	main

		pop.l	af
		cp	$0B
		jr	nz,start2
		pop.lis	ix
		ld.sis	sp,ix

start2:
		ld	hl,0
		ret.l



main:		ld	HL,MSG_HELLO
		call	PRSTR
		ret



; Print zero terminated string
; HL-points to string
PRSTR:		LD	A,(HL)		; get string character
		OR	A		; update Z flag
		RET	Z		; termination 0 found
		RST.LIS	10h		; VDU A
		INC	HL		; next char in string
		JR	PRSTR

; Data
MSG_HELLO:	.ASCII	"Hello Sailor!\n\r"
		.ASCIZ	"Implemented as MOS command in Z80 mode\r\n"

