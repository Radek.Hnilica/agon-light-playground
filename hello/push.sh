#!/bin/bash
# $Id$
# $Source$
# Push RCS versioned source back to sd card
# Copyright (c) 2023 Radek Hnilica

#set -ex

declare -r SD=/media/radek/AEDE-11B1
declare -r PROJ=hello

cp -v ${PROJ}-{prg,mos}-{z80,adl}.asm ${PROJ}-{prg,mos}-{z80,adl}.asm,v $SD/src/${PROJ}/
