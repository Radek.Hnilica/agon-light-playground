; $Id: hello-mos-adl.asm,v 1.1 2023-07-28 13:46:41+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/hello/hello-mos-adl.asm,v $
;
; MOS command E(xamine) for display memory in HEX.
; Copyright (c) 2023 Radek Hnilica
;
; BUILD & INSTALL:
;	ez80asm hello-mos-adl.asm /mos/hello.bin -lsv
;
; RUN:
;	hello
;

;---------------------------------------------------------------
; Macros for MOS and VDU call
;
; NOTE: Remember, even if we are in ADL mode, we must mangle
;	RST instruction with '.L'.

	; MOS api, the reg. A contain service number
	.MACRO	MOS fn
	LD	A,fn
	RST.L	$10
	.ENDMACRO

	; Sending one character, in register A, to VDP.
	.MACRO	VDU
	RST.L	$10
	.ENDMACRO

	.MACRO	VDU.C	char
	LD	A,char
	RST.L	$10
	.ENDMACRO

;---------------------------------------------------------------

	.FILLBYTE $00
	.ASSUME	ADL=1	; eZ80 ASL

	.ORG	$0B0000	; Where the MOS commands live in
	JR	START	; Where the real code lives

	; There is space from start of the program and MOS
	; header.  You can use it as you wish for code,
	; variables, messages, ...
	; Just don't forget not to run into MOS header!


;
;        1111111111222222222233333333334444444444555555555566666
;234567890123456789012345678901234567890123456789012345678901234
;

;---------------------------------------------------------------
; MOS program header
	; there is space for code, data up to
	.ALIGN	$40	; Header is at offset $40
	.DB	"MOS",0,1	; MOS, version 0, 1-ADL mode

;---------------------------------------------------------------
; START - the real code of the application
; IN:	HL points at CLI
; OUT:	HL contains exit code
; NOTE:	Saves necessary registers
;
START:
	PUSH	AF		; Be paranoid and save used
	PUSH	BC		;+registers.
	PUSH	DE		;+
	PUSH	IX		;+
	PUSH	IY		;+
	;EI	; Why?

	; On program/command start the HL contains address of
	; command line arguments.  So it's good idea to save it.
	LD	(ARGP),HL	; Save Argumeny Pointer to CLI
	
	LD	HL,WELCOM
	CALL	P.STR
	CALL	P.NL

EXIT:	LD	HL,0		; Return SUCCESS
	POP	IY	; Restore registers because we saved
	POP	IX	;+them.
	POP	DE	;+
	POP	BC	;+
	POP	AF	;+
	RET		; Exit, exit code is in HL
;---------------------------------------------------------------
; Memory storage for START procedure
;
ARGP:	.BLKP	1	; ^ to CLI
;
;        1111111111222222222233333333334444444444555555555566666
;234567890123456789012345678901234567890123456789012345678901234
;
WELCOM:	.ASCII	"Hello, programmer! 0.0.1 (c) 2023 Radek Hnilica\r\n"
	.ASCII	"$Id: hello-mos-adl.asm,v 1.1 2023-07-28 13:46:41+02 radek Exp radek $\r\n"
	.ASCIZ	"MOS command in ADL mode."

;---------------------------------------------------------------
; P.STR -- print zero terminated string
; IN:	HL - string address
; OUT:	print on screen
; DESTROY: A,BC, ?HL
; This version of P.STR is using VDU string call.
P.STR:
	LD	BC,0	; Zero counter to use delimiter
	XOR	A	; Get zero to A as delimiter
	RST.L	$18	; VDU string
	RET

;---------------------------------------------------------------
; P.NL  -- print newline
; IN: -
; OUT: screen
; DESTROY: A
;
P.NL:	.VDU.C	'\r'
	.VDU.C	'\n'
	RET

