# Hello program for Agon Light2 in assembler.

There is plan to have four versions of this program for combination of
ADL and Z80 mode and as standard program from $40000 and MOS command.

## Maintenance
To update code from the Agon SD card, pull files using:
```.sh
./pull.sh
```

When rcs versioned, push back using
```.sh
./push.sh
```
