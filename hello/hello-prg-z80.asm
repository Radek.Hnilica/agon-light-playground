;
; $Id: hello-prg-z80.asm,v 1.1 2023-07-24 13:36:55+02 radek Exp radek $
; $Source: /home/radek/st/repo/agonl2/src/playground/hello/hello-prg-z80.asm,v $
;
; Hello program to be loaded into standard program area $40000.
; This program variant is to be run in Z80 mode.
;
; Copyright (c) 2023 Radek Hnilica
;
; BUILD:
;	ez80asm hello-prg-z80.asm hello-prg-z80.bin -lsv
;
; EXECUTE:
;	load hello-prg-z80.bin
;	run
;

	.FILLBYTE 0
	.ASSUME	ADL=0		; Z80 memory mode
	.ORG	$0000
RST00:	JP	START

;---------------------------------------------------------------
; Trampolines for MOS and VDU calls

	; Trampoline to RST into eZ80 program space
	.ALIGN	8
RST08:	RST.L	$08	; MOS api function call
	RET
	.MACRO	MOS
	RST	8
	.ENDMACRO
	.MACRO	MOS.F	fn
	LD	A,fn
	RST	8
	.ENDMACRO

	.ALIGN	8
RST10:	RST.L	$10	; VDU character
	RET
	.MACRO	VDU
	RST	$10
	.ENDMACRO
	.MACRO	VDU.CH	ch
	LD	A,ch
	RST	$10
	.ENDMACRO

	.ALIGN	8
RST18:	RST.L	$18	; VDU string
	RET

; You can define trampoline calls to eZ80 program space if you
; want or use these restart points for whtever purposes.


;---------------------------------------------------------------

	; MOS header to tell MOS in which mode to run
	.ALIGN	$40	; must start at offset $40 from load adr
MOSHDR:	.DB	"MOS",0,0	; MOS, ver 0, Z80 mode

;---------------------------------------------------------------

START:
	LD	HL,M.HELLO
	CALL	P.STR

	; Return to MOS with success
	LD	HL,0
	RET.L


;---------------------------------------------------------------
; DATA AREA
M.HELLO:	.ASCII	"Hello, Friend!\r\n"
		.ASCII	"Implemeted as program for $40000 in"
		.ASCIZ	"Z80 mode.\r\n"

;---------------------------------------------------------------
; Subroutins and functions
; Add your ones here, or you can include them from your library
; files.

; Print zero terminated string
; Input:
;	HL	address of zero terminated string
; Output:
; Destroyed:	HL,AF

P.STR:
@@:	LD	A,(HL)
	OR	A
	RET	Z
	.VDU
	INC	HL
	JR	@B

