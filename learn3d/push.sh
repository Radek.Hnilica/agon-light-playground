#!/bin/bash
# $Id$
# $Source$
# Push RCS versioned source back to sd card
# Copyright (c) 2023 Radek Hnilica

set -ex

declare CPOPTS=-v

# The push must be directly to SD card.
declare -r SD=/media/radek/AEDE-11B1
declare -r PROJ=learn3d

cp $COPTS README.md *.{BAS,'BAS,v'}	$SD/src/${PROJ}/
