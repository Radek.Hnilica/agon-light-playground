#!/bin/bash
# $Id: pull.sh,v new $
# $Source: asmlib $
# Pull source, listing, symbols, and binary from sd card or its backup.
# Copyright (c) 2023 Radek Hnilica

set -xe
declare CPOPTS=-v

#declare -r SD=/media/radek/AEDE-11B1
declare -r SD=$HOME/st/repo/agonl2/backup
declare -r PROJ=learn3d

cp $CPOPTS $SD/src/${PROJ}/*.BAS $SD/src/${PROJ}/README.md .
#cp $CPOPTS $SD/src/${PROJ}/test/{*.asm,*.lst,README.md} test/
#cp $CPOPTS $SD/src/${PROJ}/*.inc $SD/src/${PROJ}/README.md .
