#!/usr/bin/perl -w
# $Id: srv-fork.pl,v 1.0 2023/07/05 22:45:22 radekhnilica Exp radekhnilica $
# $Source: /Users/radekhnilica/st/lrn/perl/example/net/tcp-cli-srv2/srv-fork.pl,v $
# Simple server to test communication between Agon Light2 and UNIX server.
# Based on ~/st/lrn/perl/example/net/tcp-cli-srv2/srv-fork.pl
# Copyright (c) 2023 Radek Hnilica.
#
# Start the server './srv.pl', and then use bin/tester.bin
#
# Notice the tester.bin is not perfect.  There is problem when
# switching between sending and receiving text to UART.  The tester
# solves this by waiting for final status message: OK, ERROR, or FAIL.
# srv.pl was modified to send such status after each command.  Then
# the communication become more reliable.
use strict;
use warnings;
use English;
use IO::Socket::INET;

$OUTPUT_AUTOFLUSH = 1;

# Parameters
my $port = shift || 1024;
my $server = '';	# ip to bind to
my $running = 1;

# creating a listening Socket
my $socket = new IO::Socket::INET(
    LocalHost => $server,
    LocalPort => $port,
    Proto => 'tcp',
    Listen => SOMAXCONN,
    ) or die "Cannot create socket. $!!\n";

# FIXME: get the bind address of the server
my $srvip = $socket->sockhost();

warn "Server waiting for client connections on $srvip port ${port}\n";

while (1) {
    my $session = $socket->accept() or next;

    # Fork the server
    #
    # We need to address the issue of multiple clients connecting to
    # our server.  In actual implementation the server can server only
    # one client at a time and can move to another client when the
    # actual one disconnect.
    #
    # As the using of Perl threads is discouraged, I try first with
    # fork.  It is very simple and I only add three lines of code and
    # reorganised the while loop a little bit.  Please keep on mind
    # than when forking, the child is running in separate memory space
    # and thus do not share any variable with the parent server
    # program.  This means that the DIE command will have no effect on
    # the server and only the child will die.  So the command is
    # superfluous.
    my $kid;
    next if $kid = fork;
    die "fork: $!\n" unless defined $kid;

    # Child
    #
    # Following code runs only in child process.  First things we are
    # supposed to do is to disconnect from some resources child
    # inherit from server process.
    close($socket);

    my $srvip = $session->sockaddr();
    print "Server ip $srvip\n";
    
    # Interrogate the client for log
    my $client_address = $session->peerhost();
    my $client_port = $session->peerport();
    print "Incoming connection from $client_address:$client_port\n";

    # Tell the client who we are.
    print $session "TCPSRV child=$PROCESS_ID\r\nOK\r\n";

    # Now the client is supposed to send command.  Command is string
    # terminated by and possible newline character or combination of
    # characters.
    #
    # In our simplified example we expect simple word as command
    # without any additional text or parameters.  Read commands from
    while (my $command = <$session>) {
	# chomp $command;	# Does not work with network CRLF.
	$command =~ s/\R//;	# Chomp all unicode file endings.

	if ($command =~ /^DIE$/i) {
	    print "Instructed to die by client $client_address:$client_port\n";
	    print $session "Good bye cruel $client_address!\n";
	    $running = 0;
	    last;
	}
	elsif ($command =~ /^BYE$/i) {
	    print "Bye $client_address:$client_port.\n";
	    print $session "Bye, bye.\r\nOK\r\n";
	    last;
	}
	elsif ($command =~ /^PING$/i) {
	    print $session "PONG\r\nOK\r\n";
	}
	elsif ($command =~ /^kick$/i) {
	    print $session "KICK BACK\r\n\r";
	}
	else {
	    print "RCVD $client_address:$client_port, '$command'";
	    print $session "Unknown command '$command'!\r\nERROR\r\n";
	}
    }
    print "Going to terminate the child $PROCESS_ID...";
    shutdown($session, 1);
    close($session);
    print "done.\n";
    exit 0;			# Terminate the spawned server.
}
exit 0;
