#!/bin/bash
# $Id$
# $Source$
# Push RCS versioned source back to sd card
# Copyright (c) 2023 Radek Hnilica

#set -ex

declare -r SD=/media/radek/AEDE-11B1
declare -r PROJ=mosmon

cp -v examine.asm examine.asm,v $SD/src/${PROJ}/
