# Collection of Monitor like programs for MOS
I plan to add few commands there which will add functionality like memory examining, memory editing, program tracing, ... here.

After some initial experiments to implement MOS command in Z80 mode I gave up.  Too much hassle.  So I start fresh and in ADL mode.

# Programs
- (e)xamine -- display content of memory -- partially implemented
- (m)modify -- modify content of memory -- and idea
- (d)isassemble -- disassemble content of memory -- and idea, pretty tough
- (a)ssemble -- assemble from terminal, and C128 has that -- pretty wild idea regarding eZ80.

# Examine
The binary is called examine, however when copying into /mos directory I name it e.bin.  See also the BUILD & INSTALL note in the source file.

Usage:
```txt
*e <addr>
*e <addr> <count>   -- not yet implemented
*e ?
```
