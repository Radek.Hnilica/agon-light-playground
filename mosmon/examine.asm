; $Id: examine.asm,v 0.4 2023-08-04 14:59:06+02 radek Exp $
; $Source: /home/radek/st/repo/agonl2/src/playground/mosmon/examine.asm,v $
;
; MOS command E(xamine) for display memory in HEX.
; Copyright (c) 2023 Radek Hnilica
;
; BUILD & INSTALL:
;	ez80asm examine.asm /mos/e.bin -lsv
;
; RUN:
;	e ?
;	e addr
;	e addr len
;

;---------------------------------------------------------------
; Macros for MOS and VDU call
;
; NOTE: Remember, even if we are in ADL mode, we must mangle
;	RST instruction with '.L'.

	; MOS api, the reg. A contain service number
	.MACRO	MOS fn
	LD	A,fn
	RST.L	$10
	.ENDMACRO

	; Sending one character, in register A, to VDP.
	.MACRO	VDU
	RST.L	$10
	.ENDMACRO

	.MACRO	VDU.C	char
	LD	A,char
	RST.L	$10
	.ENDMACRO

;---------------------------------------------------------------
	.FILLBYTE $00
	.ASSUME	ADL=1	; eZ80 ASL

	.ORG	$0B0000	; Where the MOS commands belongs to
	JR	START	; Where the real code lives

	; There is space from start of the program and MOS
	; header.

;---------------------------------------------------------------
; MOS program header
	; there is space for code, data up to
	.ALIGN	$40	; Header is at offset $40
	.DB	"MOS",0,1	; MOS, version 0, 1-ADL mode

;---------------------------------------------------------------
; START - the real code of the application
; IN:	HL points at CLI
; OUT:	HL contains exit code
; NOTE:	Saves necessary registers
;
START:
	PUSH	AF		; Be paranoid and save used
	PUSH	BC		;+registers.
	PUSH	DE		;+
	PUSH	IX		;+
	PUSH	IY		;+
	;EI	; Why?
	LD	(ARGP),HL	; Save Argumeny Pointer to CLI
	

	; Check if CLI starts with ? and display help message.
	CALL	SKIP_BL
	LD	HL,(ARGP)
	LD	A,(HL)
	CP	'?'
	JR	NZ,@F
	LD	HL,M.HELP
	CALL	P.CSTR
	JR	EXIT
@@:
	; Time to read hexadecimal number in CLI
	LD	IX,(ARGP)
	CALL	XTOU24		; the result is in HL
	LD	(ADR),HL	; Store the read address

	;*TODO: Read the count

	; Lets display memory from address in HL
	LD	B,16
	CALL	MEM_DMP

EXIT:	LD	HL,0		; Return SUCCESS
	POP	IY	; Restore registers because we saved
	POP	IX	;+them.
	POP	DE	;+
	POP	BC	;+
	POP	AF	;+
	RET		; Exit, exit code is in HL
;---------------------------------------------------------------
; Memory storage for START procedure
;
ARGP:	.BLKP	1	; ^ to CLI
ADR:	.BLKP	1	; Workink address

M.RESULT:	.ASCIZ	"RESULT: "

;---------------------------------------------------------------
; - skip blank characters on command line
SKIP_BL:
	LD	IY,(ARGP)	; Load ARGP
@LOOP:	LD	A,(IY+0)	; Get character from CLI
	CP	' '		; Is it SPACE
	JR	Z,@NEXT		;+YES: Next character
@EXIT:	LD	(ARGP),IY	; Store ARGP back
	RET

@NEXT:	INC	IY		; next character on CLI
	JR	@LOOP

;---------------------------------------------------------------
; MEM_DMP	- dump B bytes from address eHL
; IN:	eHL,B
; OUT:	eHL,screen
; DESTROY: A,F
;
MEM_DMP:
	PUSH	BC
	CALL	P.HEX24
	.VDU.C	':'
	CALL	P.MEM8	; HL,B
	POP	BC
	JP	P.NL	; tail call

;---------------------------------------------------------------
; Printing numbers in hexadecimal

	.INCLUDE "/src/asmlib/P.HEX24.ASM"
	.INCLUDE "/src/asmlib/P.HEX16.ASM"
	.INCLUDE "/src/asmlib/P.HEX8.ASM"

;---------------------------------------------------------------
; P.HEX8S - safe variant of P.HEX8 which preserve A
; IN:	A
; OUT:	screen
; DESTROY: -
; DEPENDS: P.HEX8
;
P.HEX8S:
	PUSH	AF
	CALL	P.HEX8
	POP	AF
	RET
;
;
;        1111111111222222222233333333334444444444555555555566666
;234567890123456789012345678901234567890123456789012345678901234
;


;---------------------------------------------------------------
; DEBUG SUBROUTINES.  REMOVE BEFORE PUTTNG INTO PRODUCTION!!!
;---------------------------------------------------------------
;
SHOW.HL:
	PUSH	HL
		LD	HL,@M.HL
		CALL	P.CSTR
	POP	HL
	CALL	P.HEX24
	RET

@M.HL:	.ASCIZ	"HL:"
;---------------------------------------------------------------
;
SHOW.IY:
	PUSH	HL
		LD	HL,@M.IY
		CALL	P.CSTR
		PUSH	IY
		POP	HL
		CALL	P.HEX24
	POP	HL
	.VDU.C	' '
	RET

@M.IY:	.ASCIZ	"IY:"

;---------------------------------------------------------------
; Data
M.HELP:
	.ASCII	"E(xamine) 0.1.2 (c) 2023 Radek Hnilica\r\n"
	.ASCII	"$Id: examine.asm,v 0.4 2023-08-04 14:59:06+02 radek Exp $\r\n"
	.ASCII	"Usage:\r\n"
	.ASCII	"\te addr\r\n"
	.ASCII	"\te addr count NYI\r\n"
	.ASCII	"\te ?\tdisplay little help\r\n"
	.ASCIZ	"Implemented as MOS command in ADL mode\r\n"


;---------------------------------------------------------------
; Load subroutines from library
	.INCLUDE "/src/asmlib/xtou24.asm"
	.INCLUDE "/src/asmlib/toupper.asm"
	.INCLUDE "/src/asmlib/isxdigit.asm"
	.INCLUDE "/src/asmlib/p.nl.asm"
	.INCLUDE "/src/asmlib/p.cstr.asm"
	;.INCLUDE "/src/asmlib/p.mem.asm"
	.INCLUDE "/src/asmlib/p.mem8.asm"
