#!/bin/bash
# $Id$
# $Source$
# Pull source, listing, symbols, and binary from sd card.
# Copyright (c) 2023 Radek Hnilica

#set -x

# declare -r SD=/media/radek/AEDE-11B1
declare -r SD=$HOME/st/repo/agonl2/backup
declare -r PROJ=mosmon

cp -v $SD/src/${PROJ}/examine.{asm,lst,symbols,bin} .
#cp -v $SD/src/${PROJ}/*.inc .
